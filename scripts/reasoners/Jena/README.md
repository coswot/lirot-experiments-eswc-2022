# Test scripts for Jena

This folder contains the scripts that were used to run experimentations with Jena.

## Setup

To setup the scripts, in this directory run the following commands:

```bash
git submodule init
git submodule update
cd tstime
make
cd ..
chmod +x test.sh
chmod +x launch_all_tests.sh
```

The Jena project also needs to be built. To do so, we used [Apache Netbeans](https://netbeans.apache.org/).  
First, install Netbeans using the procedure indicated on the website, or using your Linux distribution's standard package installation procedure. Then open the Jena project in Netbeans. In the tree view, right click on the Jena project and click "Build with Dependencies". Once this operation is done, Jena is ready to be used.

## Run the scripts

To run the tests, after completing the setup step, execute the following command:

```bash
./launch_all_tests.sh
```

As all tests are run at once, this command may take a lot of time to execute.
