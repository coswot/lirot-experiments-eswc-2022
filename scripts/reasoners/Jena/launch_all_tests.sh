#!/bin/bash

if [ -d "results" ]; then rm -r "results"; fi
mkdir "results"

mkdir "results/RDFS-Simple"
mkdir "results/RDFS-Default"
mkdir "results/RDFS-Full"

./test.sh 20 rulesets/rdfs-simple.rules  ../../../dataset-ttl/ results/RDFS-Simple/
./test.sh 20 rulesets/rdfs-default.rules  ../../../dataset-ttl/ results/RDFS-Default/
./test.sh 20 rulesets/rdfs-full.rules  ../../../dataset-ttl/ results/RDFS-Full/
