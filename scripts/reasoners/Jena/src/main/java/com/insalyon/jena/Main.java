/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.insalyon.jena;

import java.io.IOException;
import java.util.List;
import org.apache.jena.rdf.model.InfModel;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.reasoner.rulesys.GenericRuleReasoner;
import org.apache.jena.reasoner.rulesys.Rule;
import org.apache.jena.reasoner.rulesys.RuleReasoner;
import org.apache.jena.riot.RDFDataMgr;

/**
 *
 * @author alexandre
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        Model model = RDFDataMgr.loadModel(args[1]);
        model.add(RDFDataMgr.loadModel(args[2]));
        List rules = Rule.rulesFromURL(args[0]);
        RuleReasoner reasoner = new GenericRuleReasoner(rules);
        InfModel inf = ModelFactory.createInfModel(reasoner, model);
        
        if (args.length > 3)
        {
            if (args[3].equals("add"))
            {
                long start = System.currentTimeMillis();
                
                for (int i = 4; i < args.length; i++)
                {                    
                    Model addModel = RDFDataMgr.loadModel(args[i]);
                    inf.add(addModel);
                }
            }
            else if (args[3].equals("delete"))
            {
                long start = System.currentTimeMillis();
                
                for (int i = 4; i < args.length; i++)
                {
                    Model deleteModel = RDFDataMgr.loadModel(args[i]);
                    inf.remove(deleteModel);
                }
            }
            else if (args[3].equals("add-delete"))
            {
                long start = System.currentTimeMillis();
                
                for (int i = 4; i < args.length; i++)
                {                    
                    Model addDeleteModel = RDFDataMgr.loadModel(args[i]);
                    inf.add(addDeleteModel);
                    inf.remove(addDeleteModel);
                }
            }
        }
    }
    
}
