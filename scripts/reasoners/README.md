# reasoners

This folder contains the scripts to launch tests on all reasoners used in these experiments. Each reasoner has its own subfolder with instructions to launch experiments. Details about the structures of the results files can be found [here](results-csv/README.md).
