#include "Fact.h"

#ifndef KNOWLEDGEBASENODE_H_INCLUDED
#define KNOWLEDGEBASENODE_H_INCLUDED

/*
* KnowledgeBaseNode: element in a KnowledgeBase.
*	- data: pointer to the Fact element
*	- next: pointer to the next fact in the list
*/
typedef struct KnowledgeBaseNode
{
	Fact* data;
	struct KnowledgeBaseNode* next;
} KnowledgeBaseNode;

#endif // !KNOWLEDGEBASENODE_H_INCLUDED