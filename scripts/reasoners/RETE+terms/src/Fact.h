#include <stdbool.h>
#include <string.h>
#include "Utils.h"
#include "Verbose.h"
#include <stdio.h>
#include "sord/sord.h"

#ifndef FACT_H_INCLUDED
#define FACT_H_INCLUDED

// ---------------------------------------------- Structures ---------------------------------------------------

struct Term;

typedef enum TermType
{
    URI,
    BLANK,
    LITERAL,
    VARIABLE
} TermType;

typedef struct LiteralMetadata
{
    struct Term *datatype;
    char* lang;
} LiteralMetadata;

typedef struct Term
{
    char* value;
    TermType type;
    LiteralMetadata* meta;
} Term;

/**
 * Structure representing a RDF triple.
 * Variables must start with a "?" symbol.
 * Example: ?u rdf:type owl:Class
 * --> Fact
 * {
 *   subject: "?u";
 *   predicate: "rdf:type";
 *   object: "owl:Class";
 * }
 */
typedef struct Fact
{
    Term* subject;
    Term* predicate;
    Term* object;
} Fact;

typedef struct ImplicitFact
{
    Fact* value;
    LinkedList* origins; // LinkedList<BetaMatch*>
} ImplicitFact;

// ---------------------------------------------- Functions ----------------------------------------------------

// --------------- Fact functions -----------------

/**
 * @related Fact
 * Creates a fact structure from a (subject, predicate, object) triple.
 * @param copy if true, then new memory space will be allocated for the attributes of the fact; otherwise, the same pointers will be used.
 */
Fact* createFact(Term* subject, Term* predicate, Term* object);

Term* createTerm(char* value, TermType type, LiteralMetadata* meta, bool copy);

LiteralMetadata* createLiteralMetadata(Term* datatype, char* lang, bool copy);

/**
 * @relates Fact
 * Creates a clone of a Fact.
 * @param copy if true, then new memory space will be allocated for the attributes of the fact; otherwise, the same pointers will be used.
 * \return A copy of the original Fact.
 */
Fact* copyFact(Fact* fact, bool content, bool copyTerms);

Term* copyTerm(Term* term, bool copy);

LiteralMetadata* copyLiteralMetadata(LiteralMetadata* meta, bool copy);

/**
 * @relates Fact
 * Displays a Fact.
 */
void printFact(Fact* f);

/**
 * @relates Fact
 * Displays a SordQuad.
 */
void printSordQuad(SordQuad quad);

/**
 * \relates Fact
 * Returns true if f1 and f2 have the same subject, predicate and object; false otherwise.
 */
bool factEquals(Fact* f1, Fact* f2);

bool termEquals(Term* t1, Term* t2);

bool matchAtom(Fact* atom, Fact* f);

bool matchTwoAtoms(Fact* a1, Fact* a2);

bool checkAtomsInclusion(Fact* a1, Fact* a2);

/**
 * \relates Fact
 * Extracts a Fact from text.
 * @param atom text with the following pattern: "subject,predicate,object".
 */
Fact* getFactFromAtom(char* atom, bool copy);

Term* getTermFromSordNode(const SordNode* node, bool copy);

SordNode* getSordNodeFromTerm(Term* t, SordWorld* world);

ImplicitFact* createImplicitFact(Fact* value);

// ----------------- Destructors ------------------

/**
 * \relates Fact
 * Frees dynamic memory inside a Fact.
 * @param content if true, frees memory space allocated to its attributes.
 */
void destroyFact(Fact* f, bool content, bool destroyTerms);

void destroyTerm(Term* t, bool content);

void destroyTermWithoutPointer(Term* t);

void destroyLiteralMetadata(LiteralMetadata* meta, bool content);

void destroyImplicitFact(ImplicitFact* impl);


#endif // !FACT_H_INCLUDED