#include "Fact.h"
#include "FactLinkedList.h"

#ifndef RULE_H_INCLUDED
#define RULE_H_INCLUDED

// ---------------------------------------------- Structures ---------------------------------------------------

/**
 * A logic rule of the form (condition_1), (condition_2), .\ .\ .\ , (condition_n) --> (head).
 * Example: (?v rdfs:subClassOf ?w), (?u rdf:type ?v) --> (?u rdf:type ?w)
 * 
 * \code{.c}
 * Rule
 * {
 *   body: LinkedList<Fact*>{&Fact(?v rdfs:subClassOf ?w), &Fact(?u rdf:type)};
 *   head: &Fact(?u rdf:type ?w);
 * }
 * \endcode
*/
typedef struct Rule
{
    /**
     * Conditions of the rule.
     */
    FactLinkedList* body;

    /**
     * Conclusion of the rule.
     */
    Fact* head;
} Rule;

// ------------------------------------------------ Functions --------------------------------------------------

/**
 * \relates Rule
 * Extract rules from text (see rule format in the readme).
 * @param lines List of lines in the input text file.
 */
LinkedList* getRules(LinkedList* lines);

/**
 * \relates Rule
 * Creates a rule.
 * @param body Conditions of the rule.
 * @param head Conclusion of the rule.
 */
Rule* createRule(FactLinkedList* body, Fact* head);

/**
 * \relates Rule
 * Displays a rule.
 */
void printRule(Rule* r);

/**
 * \relates Rule
 * Frees dynamic memory allocated to a rule.
 */
void destroyRule(Rule* r);

#endif // !FACT_H_INCLUDED