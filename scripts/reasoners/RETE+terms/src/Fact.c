#include "Fact.h"

Term* createTerm(char* value, TermType type, LiteralMetadata* meta, bool copy)
{
    Term* term = (Term*)malloc(sizeof(Term));
    if (term)
    {
        term->meta = NULL;

        if (copy)
        {
            term->value = initString(value);
        }
        else
        {
            term->value = value;
        }
        
        term->type = type;
        term->meta = meta;
    }

    return term;
}

LiteralMetadata* createLiteralMetadata(Term* datatype, char* lang, bool copy)
{
    LiteralMetadata* meta = (LiteralMetadata*)malloc(sizeof(LiteralMetadata));
    if (meta)
    {
        meta->datatype = datatype;
        if (copy)
        {
            meta->lang = lang == NULL ? NULL : initString(lang);
        }
        else
        {
            meta->lang = lang;
        }
    }

    return meta;
}

Fact* createFact(Term* subject, Term* predicate, Term* object)
{
    Fact* result = (Fact*)malloc(sizeof(Fact));
    if (result)
    {
        result->subject = subject;
        result->predicate = predicate;
        result->object = object;
    }

    if (verboseOn)
    {
        printf("Created ");
        printFact(result);
    }

    return result;
}

Fact* copyFact(Fact* fact, bool content, bool copyTerms)
{
    Fact* newFact = (Fact*)malloc(sizeof(Fact));
    if (newFact)
    {
        if (copyTerms)
        {
            newFact->subject = copyTerm(fact->subject, content);
            newFact->predicate = copyTerm(fact->predicate, content);
            newFact->object = copyTerm(fact->object, content);
        }
        else
        {
            newFact->subject = fact->subject;
            newFact->predicate = fact->predicate;
            newFact->object = fact->object;
        }
    }

    return newFact;
}

Term* copyTerm(Term* term, bool copy)
{
    Term* newTerm = (Term*)malloc(sizeof(Term));
    if (newTerm)
    {
        if (copy)
        {
            newTerm->value = initString(term->value);
        }
        else
        {
            newTerm->value = term->value;
        }

        newTerm->type = term->type;

        if (term->meta)
        {
            newTerm->meta = copyLiteralMetadata(term->meta, copy);
        }
        else
        {
            newTerm->meta = NULL;
        }
    }

    return newTerm;
}

LiteralMetadata* copyLiteralMetadata(LiteralMetadata* meta, bool copy)
{
    LiteralMetadata* newMeta = (LiteralMetadata*)malloc(sizeof(LiteralMetadata));
    if (newMeta)
    {
        if (meta->lang)
        {
            if (copy)
            {
                newMeta->lang = initString(meta->lang);
            }
            else
            {
                newMeta->lang = meta->lang;
            }
        }
        else
        {
            newMeta->lang = NULL;
        }
        
        if (meta->datatype)
        {
            newMeta->datatype = copyTerm(meta->datatype, copy);
        }
        else
        {
            newMeta->datatype = NULL;
        }
    }

    return newMeta;
}

bool termEquals(Term* t1, Term* t2)
{
    bool res = (t1 == t2);
    if (!res)
    {
        res = (strcmp(t1->value, t2->value) == 0);

        res &= (t1->type == t2->type);

        if (res && t1->type == LITERAL)
        {
            // If both meta or null
            if (t1->meta == NULL && t2->meta == NULL)
            {
                res = true;
            }
            // If one meta is null and the other is not
            else if ((t1->meta == NULL && t2->meta != NULL) || (t2->meta == NULL && t1->meta != NULL))
            {
                res = false;
            }
            // If both meta are non null
            else
            {
                // If both langages are null
                if (t1->meta->lang == NULL && t2->meta->lang == NULL)
                {
                    res = true;
                }
                // If one language is null and the other is not
                else if ((t1->meta->lang == NULL && t2->meta->lang != NULL) || (t2->meta->lang == NULL && t1->meta->lang != NULL))
                {
                    res = false;
                }
                // If both languages are non null
                else
                {
                    if (strcmp(t1->meta->lang, t2->meta->lang) != 0)
                    {
                        res = false;
                    }
                }
                if (res)
                {
                    // If both datatypes are null
                    if (t1->meta->datatype == NULL && t2->meta->datatype == NULL)
                    {
                        res = true;
                    }
                    // If one datatype is null and the other is not
                    else if ((t1->meta->datatype == NULL && t2->meta->datatype != NULL) || (t2->meta->datatype == NULL && t1->meta->datatype != NULL))
                    {
                        res = false;
                    }
                    else
                    {
                        if (!termEquals(t1->meta->datatype, t2->meta->datatype))
                        {
                            res = false;
                        }
                    }
                }
            }
        }
    }
    
    

    return res;
}

bool factEquals(Fact* f1, Fact* f2)
{
    bool res = termEquals(f1->subject, f2->subject) && termEquals(f1->predicate, f2->predicate) && termEquals(f1->object, f2->object);

    return res;
}

void printFact(Fact* f)
{
    printf("Fact: %s %s %s\n", f->subject->value, f->predicate->value, f->object->value);
}

void printSordQuad(SordQuad quad)
{
    printf("Quad: %s %s %s\n", sord_node_get_string(quad[0]), sord_node_get_string(quad[1]), sord_node_get_string(quad[2]));
}

Fact* getFactFromAtom(char* atom, bool copy)
{
	char factDelim[] = ",";
	LinkedList* terms = splitStr(atom, factDelim);

    char* subjectString = (char*)getLinkedListAtIndex(terms, 0);
	Term* subject = createTerm(subjectString, startsWith(subjectString, "?") ? VARIABLE : URI, NULL, copy);

    char* predicateString= (char*)getLinkedListAtIndex(terms, 1);
	Term* predicate = createTerm(predicateString, startsWith(predicateString, "?") ? VARIABLE : URI, NULL, copy);

    char* objectString = (char*)getLinkedListAtIndex(terms, 2);
	Term* object = createTerm(objectString, startsWith(objectString, "?") ? VARIABLE : URI, NULL, copy);

	Fact* f = createFact(subject, predicate, object);

    if (copy)
    {
        LinkedListNode* curr = terms->first;
        while (curr != NULL)
        {
            free(curr->data);
            curr = curr->next;
        }
    }

	freeLinkedList(terms);

	return f;
}

Term* getTermFromSordNode(const SordNode* node, bool copy)
{
    SordNodeType type = sord_node_get_type(node);
    // printf("Type: %d\n", type);
    char* lang = (char*)sord_node_get_language(node);
    SordNode* datatype = sord_node_get_datatype(node);
    char* value = (char*)sord_node_get_string(node);

    LiteralMetadata* meta = NULL;
    Term* term = (Term*)malloc(sizeof(Term));
    term->meta = NULL;
    term->value = (char*)sord_node_get_string(node);

    if (type == SORD_LITERAL)
    {
        Term* termDatatype = NULL;
        if (datatype != NULL)
        {
            termDatatype = createTerm((char*)sord_node_get_string(datatype), URI, NULL, copy);
        }

        term->type = LITERAL;
        term->meta = createLiteralMetadata(termDatatype, lang, copy);
    }
    else if (type == SORD_URI)
    {
        term->type = URI;
    }
    else if (type == SORD_BLANK)
    {
        term->type = BLANK;
    }

    return term;
}

SordNode* getSordNodeFromTerm(Term* t, SordWorld* world)
{
    SordNode* result = NULL;
    uint8_t* value = (uint8_t*)t->value;
    //printf("Type %d\n", t->type);
    switch (t->type)
    {
    case URI:
        result = sord_new_uri(world, value);
        break;
    
    case BLANK:
        result = sord_new_blank(world, value);
        break;
    
    case LITERAL:
        {
            uint8_t* lang = t->meta->lang != NULL ? (uint8_t*)t->meta->lang : NULL;
            SordNode* datatype = t->meta->datatype != NULL ? sord_new_uri(world, (uint8_t*)t->meta->datatype->value) : NULL;
            result = sord_new_literal(world, datatype, value, lang);
            break;
        }
    
    default:
        break;
    }

    return result;
}

bool matchAtom(Fact* atom, Fact* f)
{
    bool match = true;

    // Subject
    if (atom->subject->type != VARIABLE && !termEquals(atom->subject, f->subject))
    {
        match = false;
    }

    // Predicate
    if (atom->predicate->type != VARIABLE && !termEquals(atom->predicate, f->predicate))
    {
        match = false;
    }

    // Object
    if (atom->object->type != VARIABLE && !termEquals(atom->object, f->object))
    {
        match = false;
    }

    return match;
}

bool matchTwoAtoms(Fact* a1, Fact* a2)
{
    bool match = true;

    // Subject
    if (a1->subject->type != VARIABLE && a2->subject->type != VARIABLE && !termEquals(a1->subject, a2->subject))
    {
        match = false;
    }
    if ((a1->subject->type == VARIABLE && a2->subject->type != VARIABLE) || (a2->subject->type == VARIABLE && a1->subject->type != VARIABLE))
    {
        match = false;
    }

    // Predicate
    if (a1->predicate->type != VARIABLE && a2->predicate->type != VARIABLE && !termEquals(a1->predicate, a2->predicate))
    {
        match = false;
    }
    if ((a1->predicate->type == VARIABLE && a2->predicate->type != VARIABLE) || (a2->predicate->type == VARIABLE && a1->predicate->type != VARIABLE))
    {
        match = false;
    }

    // Object
    if (a1->object->type != VARIABLE && a2->object->type != VARIABLE && !termEquals(a1->object, a2->object))
    {
        match = false;
    }
    if ((a1->object->type == VARIABLE && a2->object->type != VARIABLE) || (a2->object->type == VARIABLE && a1->object->type != VARIABLE))
    {
        match = false;
    }

    return match;
}

bool checkAtomsInclusion(Fact* a1, Fact* a2)
{
    bool match = true;

    // Subject
    if (a1->subject->type != VARIABLE && !termEquals(a1->subject, a2->subject))
    {
        match = false;
    }
    // Predicate
    if (a1->predicate->type != VARIABLE && !termEquals(a1->predicate, a2->predicate))
    {
        match = false;
    }
    // Object
    if (a1->object->type != VARIABLE && !termEquals(a1->object, a2->object))
    {
        match = false;
    }

    return match;
}


ImplicitFact* createImplicitFact(Fact* value)
{
    ImplicitFact* result = (ImplicitFact*)malloc(sizeof(ImplicitFact));
    if (result)
    {
        result->value = value;
        result->origins = createLinkedList();
    }

    return result;
}

void destroyFact(Fact* f, bool content, bool destroyTerms)
{
    if (destroyTerms)
    {
        destroyTerm(f->subject, content);
        destroyTerm(f->predicate, content);
        destroyTerm(f->object, content);
    }
    
    free(f);
}

void destroyTerm(Term* t, bool content)
{
    if (content)
    {
        free(t->value);
    }

    if (t->meta)
    {
        destroyLiteralMetadata(t->meta, content);
    }

    free(t);
}

void destroyTermWithoutPointer(Term* t)
{
    // free(t->value);
    if (t->meta)
    {
        destroyLiteralMetadata(t->meta, false);
    }
}

void destroyLiteralMetadata(LiteralMetadata* meta, bool content)
{
    if (content && meta->lang)
    {
        free(meta->lang);
    }
    if (meta->datatype)
    {
        destroyTerm(meta->datatype, content);
    }

    free(meta);
}

void destroyImplicitFact(ImplicitFact* impl)
{
    freeLinkedList(impl->origins);
    free(impl);
}