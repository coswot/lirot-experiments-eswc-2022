#include "Rete.h"

Rete* createReteNetwork(LinkedList* rules, SordModel* kb)
{
    if (verboseOn)
    {
        printf("Enter function createReteNetwork.\n");
    }

    Rete* rete = (Rete*)malloc(sizeof(Rete));
    if (rete)
    {
        rete->kb = kb;
        rete->rules = rules;
        rete->alphaNodes = createLinkedList();
        rete->betaNodes = createLinkedList();

        rete->terms = NULL;

        // Alpha nodes
        // One alpha node per rule atomic condition
        LinkedListNode* currRule = (LinkedListNode*)rules->first;
        int count = 0;
        while (currRule != NULL)
        {
            Rule* r = (Rule*)currRule->data;
            LinkedListNode* currBody = (LinkedListNode*)r->body->first;
            while (currBody != NULL)
            {
                Fact* a = (Fact*)currBody->data;
                pushLinkedList(rete->alphaNodes, createAlphaNode(a, r));
                count++;
                currBody = currBody->next;
            }

            currRule = currRule->next;
        }

        if (verboseOn)
        {
            printf("Created %d AlphaNodes.\n", count);
        }

        //Beta nodes
        // First b beta node joins the first two alpha nodes of b rule
        // Then b beta node joins the previous one with the following alpha node, etc.
        // until no alpha node remains disconnected.
        currRule = rules->first;
        count = 0;
        while (currRule != NULL)
        {
            Rule* r = (Rule*)currRule->data;
            // Find all alpha nodes by rule
            LinkedList* alpha_r = findAlphaNodesByRule(rete, r);

            if (r->body->size == 1)
            {
                AlphaNode* a = (AlphaNode*) alpha_r->first->data;
                pushLinkedList(rete->betaNodes, createBetaNodeFromAlpha(r, a, NULL));
                count++;
            }
            else if (r->body->size > 1)
            {
                bool first_node = true;
                LinkedListNode* currAlpha = alpha_r->first;
                
                while (currAlpha != NULL)
                {
                    AlphaNode* a = (AlphaNode*)currAlpha->data;
                    if (first_node)
                    {
                        pushLinkedList(rete->betaNodes, createBetaNodeFromAlpha(r, a, (AlphaNode*)currAlpha->next->data));
                        currAlpha = currAlpha->next;
                        first_node = false;
                        count++;
                    }
                    else
                    {
                        pushLinkedList(rete->betaNodes, createBetaNodeFromBeta(r, (BetaNode*)rete->betaNodes->last->data, a));
                        count++;
                    }

                    currAlpha = currAlpha->next;
                }
            }

            freeLinkedList(alpha_r);

            currRule = currRule->next;
        }

        // LinkedListNode* currAlpha = rete->alphaNodes->first;
        // while (currAlpha != NULL)
        // {
        //     AlphaNode* alpha = (AlphaNode*)currAlpha->data;
        //     printFact(alpha->atom);
        //     currAlpha = currAlpha->next;
        // }
        // printf("\n");

        rete->terminalBetaNodes = createLinkedList();
        LinkedListNode* currBeta = rete->betaNodes->first;
        while (currBeta != NULL)
        {
            BetaNode* beta = (BetaNode*)currBeta->data;
            if (beta->terminalNode)
            {
                pushLinkedList(rete->terminalBetaNodes, beta);
                beta->matchingAlphaNodes = rete->alphaNodes;
                // printf("Beta: ");
                // printFact(beta->rule->head);

                // LinkedListNode* currAlpha = rete->alphaNodes->first;
                // while (currAlpha != NULL)
                // {
                //     AlphaNode* alpha = (AlphaNode*)currAlpha->data;
                //     if (checkAtomsInclusion(alpha->atom, beta->rule->head))
                //     {
                //         if (beta->matchingAlphaNodes == NULL)
                //         {
                //             beta->matchingAlphaNodes = createLinkedList();
                //         }
                //         pushLinkedList(beta->matchingAlphaNodes, alpha);
                //         // printf("Alpha: ");
                //         // printFact(alpha->atom);
                //     }

                //     currAlpha = currAlpha->next;
                // }
                // printf("\n");
            }

            currBeta = currBeta->next;
        }

        if (verboseOn)
        {
            printf("Created %d BetaNodes.\n", count);
        }
    }

    if (verboseOn)
    {
        printf("Leave function createReteNetwork.\n");
    }
    
    return rete;
}

static uint32_t
sord_node_hash(const void* n)
{
  const SordNode* node = (const SordNode*)n;
  uint32_t        hash = zix_digest_start();
  hash = zix_digest_add(hash, node->node.buf, node->node.n_bytes);
  hash = zix_digest_add(hash, &node->node.type, sizeof(node->node.type));
  if (node->node.type == SERD_LITERAL) {
    hash = zix_digest_add(hash, &node->meta.lit, sizeof(node->meta.lit));
  }
  return hash;
}

Term* insertTerm(void* rete, void* node, bool destroyIfExists)
{
    // printf("Term: %s\n", term->value);
    Term* result = NULL;
    Rete* r = (Rete*)rete;
    SordNode* n = (SordNode*)node;

    HashElement* newHashElem = malloc(sizeof(HashElement));
    if (newHashElem)
    {
        int hash = sord_node_hash(node);
        newHashElem->key = hash;

        HashElement* searchResult = NULL;
        HASH_FIND_INT(r->terms, &hash, searchResult);
        // printf("Looking for term %s\n", newHashElem->term->value);
        if (searchResult)
        {
            // printf("Found term %s\n", searchResult->term->value);
            result = searchResult->term;
            free(newHashElem);
        }
        else
        {
            HASH_ADD_INT(r->terms, key, newHashElem);
            newHashElem->term = getTermFromSordNode(n, false);
            result = newHashElem->term;
        }
    }

    // int count = HASH_COUNT(r->terms);
    // printf("%d items\n", count);

    return result;
}

void addExplicitFacts(Rete* rete, SordModel* newFacts)
{
    if (verboseOn)
    {
        printf("Enter function infer.\n");
    }
    int nbFacts = sord_num_quads(rete->kb);
    bool keepInferring = true;
    bool firstIteration = true;

    SordWorld* world = sord_get_world(rete->kb);
    SordModel* kb = newFacts == NULL ? rete->kb : newFacts;

    while (keepInferring)
    {
        LinkedList* checkedAlphaNodes = createLinkedList();

        LinkedList* betaNodesToCheck = createLinkedList();

        // Check all alpha nodes
        LinkedListNode* currAlpha = rete->alphaNodes->first;
        while (currAlpha != NULL)
        {
            AlphaNode* alpha = (AlphaNode*)currAlpha->data;
            if (firstIteration)
            {
                bool checkNext = checkAlphaNode(rete, alpha, kb, insertTerm);
                if (checkNext)
                {
                    pushLinkedList(checkedAlphaNodes, alpha);
                }
            }
            else if (alpha->newMatches->size > 0)
            {
                pushLinkedList(checkedAlphaNodes, alpha);
            }
            
            currAlpha = currAlpha->next;
        }

        currAlpha = checkedAlphaNodes->first;
        while (currAlpha != NULL)
        {
            AlphaNode* alpha = (AlphaNode*)currAlpha->data;
            // alpha is the right parent of its successor
            if (alpha == alpha->successor->rightPredecessor)
            {
                // alpha's successor has two alpha parents
                if (alpha->successor->fromAlpha)
                {
                    checkBetaNodeAddFact(rete, alpha->successor, alpha->successor->alphaLeftPredecessor->matches, alpha->newMatches, alpha->successor->alphaLeftPredecessor->matches->size, alpha->oldNewMatchesSize, NULL, rete->kb, insertTerm);
                }
                // alpha's successor has a beta and an alpha parent
                else
                {
                    checkBetaNodeAddFact(rete, alpha->successor, alpha->successor->betaLeftPredecessor->matches, alpha->newMatches, alpha->successor->betaLeftPredecessor->matches->size, alpha->oldNewMatchesSize, NULL, rete->kb, insertTerm);
                }
            }
            // alpha is the left parent of its successor
            else if (alpha == alpha->successor->alphaLeftPredecessor)
            {
                // alpha's successor has two (alpha) parents
                if (alpha->successor->rightPredecessor != NULL)
                {
                    checkBetaNodeAddFact(rete, alpha->successor, alpha->newMatches, alpha->successor->rightPredecessor->matches, alpha->oldNewMatchesSize, alpha->successor->rightPredecessor->matches->size, NULL, rete->kb, insertTerm);
                }
                // alpha's successor has only one (alpha) parent
                else
                {
                    checkBetaNodeAddFact(rete, alpha->successor, alpha->newMatches, NULL, alpha->oldNewMatchesSize, 0, NULL, rete->kb, insertTerm);
                }
            }

            moveAlphaMatches(alpha);
            currAlpha = currAlpha->next;
        }

        bool foundNewAlphaMatches = false;
        currAlpha = rete->alphaNodes->first;
        while (currAlpha != NULL)
        {
            AlphaNode* alpha = (AlphaNode*)currAlpha->data;
            if (alpha->newMatches->size > 0)
            {
                foundNewAlphaMatches = true;
                break;
            }

            currAlpha = currAlpha->next;
        }

        // if (returnFacts->size > nbFacts)
        // {
        //     nbFacts = returnFacts->size;
        // }
        int nbNewFacts = sord_num_quads(kb);
        // printf("%d new facts\n", nbNewFacts - nbFacts);

        if (nbNewFacts > nbFacts)
        {
            nbFacts = nbNewFacts;
        }
        else if (!foundNewAlphaMatches)
        {
            keepInferring = false;
        }

        freeLinkedList(checkedAlphaNodes);
        freeLinkedList(betaNodesToCheck);

        firstIteration = false;
    }
}

void removeExplicitFacts(Rete* rete, SordModel* deletedFacts)
{
    int nbFacts = sord_num_quads(rete->kb);
    bool keepInferring = true;
    bool firstIteration = true;

    SordWorld* world = sord_get_world(rete->kb);

    while (keepInferring)
    {
        LinkedList* checkedAlphaNodes = createLinkedList();

        LinkedList* betaNodesToCheck = createLinkedList();

        // Check all alpha nodes
        LinkedListNode* currAlpha = rete->alphaNodes->first;
        while (currAlpha != NULL)
        {
            AlphaNode* alpha = (AlphaNode*)currAlpha->data;
            if (firstIteration)
            {
                bool checkNext = checkAlphaNodeRemoveFacts(rete, alpha, deletedFacts, insertTerm);
                if (checkNext)
                {
                    pushLinkedList(checkedAlphaNodes, alpha);
                }
            }
            else if (alpha->deletedMatches->size > 0)
            {
                pushLinkedList(checkedAlphaNodes, alpha);
            }
            
            currAlpha = currAlpha->next;
        }

        currAlpha = checkedAlphaNodes->first;
        while (currAlpha != NULL)
        {
            AlphaNode* alpha = (AlphaNode*)currAlpha->data;
            bool fromLeft = false;
            if (alpha->successor->fromAlpha && alpha->successor->alphaLeftPredecessor == alpha)
            {
                fromLeft = true;
            }
            
            checkBetaNodeRemoveFact(rete, alpha->successor, fromLeft, rete->kb, rete->terminalBetaNodes, insertTerm);

            moveAlphaMatches(alpha);
            currAlpha = currAlpha->next;
        }

        bool foundDeletedAlphaMatches = false;
        currAlpha = rete->alphaNodes->first;
        while (currAlpha != NULL)
        {
            AlphaNode* alpha = (AlphaNode*)currAlpha->data;
            if (alpha->deletedMatches->size > 0)
            {
                foundDeletedAlphaMatches = true;
                break;
            }
            currAlpha = currAlpha->next;
        }

        if (!foundDeletedAlphaMatches)
        {
            keepInferring = false;
        }

        freeLinkedList(checkedAlphaNodes);
        freeLinkedList(betaNodesToCheck);

        firstIteration = false;
    }
}

LinkedList* findAlphaNodesByRule(Rete* rete, Rule* r)
{
    LinkedList* res = createLinkedList();
    LinkedListNode* currAlpha = rete->alphaNodes->first;
    while (currAlpha != NULL)
    {
        AlphaNode* a = (AlphaNode*)currAlpha->data;
        if (a->rule == r)
        {
            pushLinkedList(res, a);
        }

        currAlpha = currAlpha->next;
    }

    return res;
}

void cleanRete(Rete* rete)
{
    // Clean alpha nodes
    LinkedListNode* currAlpha = rete->alphaNodes->first;
    while (currAlpha != NULL)
    {
        AlphaNode* a = (AlphaNode*)currAlpha->data;

        // Delete matches
        FactLinkedListNode* currFact = a->matches->first;
        while (currFact != NULL)
        {
            Fact* f = (Fact*)currFact->data;
            destroyFact(f, false, false);
            currFact = currFact->next;
        }

        currAlpha = currAlpha->next;
    }

    //Clean beta nodes
    LinkedListNode* currBeta = rete->betaNodes->first;
    while (currBeta != NULL)
    {
        BetaNode* b = (BetaNode*)currBeta->data;
        LinkedListNode* currMatch = b->matches->first;
        while (currMatch != NULL)
        {
            BetaMatch* m = (BetaMatch*)currMatch->data;
            destroyBetaMatch(m);
            currMatch = currMatch->next;
        }
        freeLinkedList(b->matches);
        b->matches = createLinkedList();

        if (b->implicitFacts != NULL)
        {
            LinkedListNode* currImpl = b->implicitFacts->first;
            while (currImpl != NULL)
            {
                ImplicitFact* impl = (ImplicitFact*)currImpl->data;
                destroyFact(impl->value, false, false);
                destroyImplicitFact(impl);
                currImpl = currImpl->next;
            }

            freeLinkedList(b->implicitFacts);
            b->implicitFacts = createLinkedList();
        }

        currBeta = currBeta->next;
    }
}

void destroyRete(Rete* rete)
{
    // Clean Rete
    cleanRete(rete);

    // Free alpha nodes
    LinkedListNode* currAlpha = rete->alphaNodes->first;
    while (currAlpha != NULL)
    {
        AlphaNode* a = (AlphaNode*)currAlpha->data;
        freeFactLinkedList(a->matches);
        freeFactLinkedList(a->newMatches);
        freeFactLinkedList(a->deletedMatches);
        free(a);
        currAlpha = currAlpha->next;
    }
    freeLinkedList(rete->alphaNodes);
    
    // Free beta nodes
    LinkedListNode* currBeta = rete->betaNodes->first;
    while (currBeta != NULL)
    {
        BetaNode* b = (BetaNode*)currBeta->data;
        freeLinkedList(b->variables->joinVariables);
        freeLinkedList(b->variables->otherVariables);
        free(b->variables);
        freeLinkedList(b->matches);
        freeLinkedList(b->newMatches);
        freeLinkedList(b->deletedMatches);

        if (b->implicitFacts != NULL)
        {
            freeLinkedList(b->implicitFacts);
        }

        // if (b->matchingAlphaNodes != NULL)
        // {
        //     freeLinkedList(b->matchingAlphaNodes);
        // }
        
        free(b);
        currBeta = currBeta->next;
    }
    freeLinkedList(rete->betaNodes);
    freeLinkedList(rete->terminalBetaNodes);

    // Free rules
    LinkedListNode* currRule = rete->rules->first;
    while (currRule != NULL)
    {
        destroyRule((Rule*)currRule->data);
        currRule = currRule->next;
    }
    freeLinkedList(rete->rules);

    struct HashElement* currentElem, *tmp;

    HASH_ITER(hh, rete->terms, currentElem, tmp)
    {
        HASH_DEL(rete->terms, currentElem);
        destroyTerm(currentElem->term, false);
        free(currentElem);
    }

    // Free rete
    free(rete);
}