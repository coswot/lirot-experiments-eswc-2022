// #include "KnowledgeBase.h"

// KnowledgeBase* createKnowledgeBase()
// {
// 	KnowledgeBase* l = (KnowledgeBase*)malloc(sizeof(KnowledgeBase));
// 	if (l)
// 	{
// 		l->size = 0;
// 		l->first = NULL;
// 		l->last = NULL;
// 	}
	

// 	return l;
// }

// bool addFactToKB(KnowledgeBase* kb, Fact* element)
// {
// 	KnowledgeBaseNode* newElem = (KnowledgeBaseNode*)malloc(sizeof(KnowledgeBaseNode));
// 	if (newElem)
// 	{
// 		newElem->next = NULL;
// 		newElem->data = element;
// 		if (kb->size == 0)
// 		{
// 			kb->first = newElem;
// 			kb->last = kb->first;
// 		}
// 		else
// 		{
// 			kb->last->next = newElem;
// 			kb->last = newElem;
// 		}

//         kb->size++;

//         return true;
// 	}
	
// 	return false;
// }

// void insertKnowledgeBaseAtIndex(KnowledgeBase* kb, Fact* element, int index)
// {
// 	if (index == 0)
// 	{
// 		KnowledgeBaseNode* newElem = (KnowledgeBaseNode*)malloc(sizeof(KnowledgeBaseNode));
// 		if (newElem)
// 		{
// 			newElem->data = element;
// 			newElem->next = kb->first;
// 			kb->first = newElem;
// 			kb->size++;
// 		}
// 	}
// 	else if (index > 0)
// 	{
// 		if (index == kb->size)
// 		{
// 			addFactToKB(kb, element);
// 		}
// 		else if (index < kb->size)
// 		{
// 			int i = 0;
// 			KnowledgeBaseNode* current = kb->first;
// 			while (i < index - 1)
// 			{
// 				current = current->next;
// 				i++;
// 			}
// 			KnowledgeBaseNode* newElem = (KnowledgeBaseNode*)malloc(sizeof(KnowledgeBaseNode));
// 			if (newElem)
// 			{
// 				newElem->data = element;
// 				newElem->next = current->next;
// 				current->next = newElem;
// 				kb->size++;
// 			}
// 		}
// 	}
// }

// bool KBHasFact(KnowledgeBase* facts, Fact* element)
// {
// 	KnowledgeBaseNode* curr = facts->first;
// 	while (curr != NULL)
// 	{
// 		if (factEquals(curr->data, element))
// 		{
// 			return true;
// 		}
// 		curr = curr->next;
// 	}

// 	return false;
// }

// void* getKnowledgeBaseAtIndex(KnowledgeBase* kb, int index)
// {
// 	Fact* res = NULL;

// 	if (index >= 0 && index < kb->size)
// 	{
// 		KnowledgeBaseNode* curr = kb->first;
// 		int i = 0;
// 		while (i < index)
// 		{
// 			curr = curr->next;
// 			i++;
// 		}

// 		res = curr->data;
// 	}

// 	return res;
// }

// void removeKnowledgeBaseAtIndex(KnowledgeBase* kb, int index)
// {
// 	if (index < kb->size)
// 	{
// 		if (index == 0)
// 		{
// 			KnowledgeBaseNode* toRemove = kb->first;
// 			kb->first = kb->first->next;
// 			free(toRemove);
// 			kb->size--;
// 		}
// 		else
// 		{
// 			KnowledgeBaseNode* current = kb->first;
// 			int i = 0;
// 			while (i < index - 1)
// 			{
// 				current = current->next;
// 				i++;
// 			}
// 			KnowledgeBaseNode* toRemove = current->next;
// 			current->next = current->next->next;
// 			free(toRemove);
// 			kb->size--;
// 		}
// 	}

// 	if (kb->size == 1)
// 	{
// 		kb->last = kb->first;
// 	}

// 	if (kb->size == 0)
// 	{
// 		kb->first = NULL;
// 		kb->last = NULL;
// 	}
// }

// void freeKnowledgeBase(KnowledgeBase* kb)
// {
// 	KnowledgeBaseNode* current = kb->first;
// 	KnowledgeBaseNode* tmp;

// 	while (current != NULL)
// 	{
// 		tmp = current;
// 		current = current->next;
// 		free(tmp);
// 	}
	
// 	free(kb);
// }

// KnowledgeBase* getFactsKB(LinkedList* lines)
// {
// 	KnowledgeBase* kb = createKnowledgeBase();

// 	LinkedListNode* currLine = lines->first;
// 	while (currLine != NULL)
// 	{
// 		Fact* f = getFactFromAtom((char*)currLine->data);
// 		addFactToKB(kb, f);

// 		currLine = currLine->next;
// 	}

// 	return kb;
// }

// char* getVariableValueKB(char* variable, KnowledgeBase* kb)
// {
//     char* value = NULL;
//     KnowledgeBaseNode* currFact = kb->first;
//     while (currFact != NULL)
//     {
//         Fact* f = currFact->data;
//         if (strcmp(f->subject, variable) == 0 && strcmp(f->predicate, "rdf:value") == 0)
//         {
//             value = f->object;
//             break;
//         }
//         currFact = currFact->next;
//     }

//     return value;
// }

// bool deleteFactFromKB(KnowledgeBase* kb, Fact* element)
// {
// 	KnowledgeBaseNode* curr = kb->first;
// 	while (curr != NULL)
// 	{
// 		if (factEquals(curr->data, element))
// 		{
// 			if (curr != kb->last)
// 			{
// 				if (curr == kb->first)
// 				{
// 					kb->first = kb->first->next;
// 					free(curr);
// 					kb->size--;
// 					break;
// 				}
// 				else
// 				{
// 					KnowledgeBaseNode* prec = kb->first;
// 					while (prec->next != curr)
// 					{
// 						prec = prec->next;
// 					}
// 					prec->next = prec->next->next;
// 					free(curr);
// 					kb->size--;
// 					break;
// 				}
// 			}
// 			else
// 			{
// 				KnowledgeBaseNode* prec = kb->first;
// 				while (prec->next != curr)
// 				{
// 					prec = prec->next;
// 				}
// 				prec->next = NULL;
// 				kb->last = prec;
// 				free(curr);
// 				kb->size--;
// 				break;
// 			}

// 			if (kb->size == 1)
// 			{
// 				kb->last = kb->first;
// 			}

// 			if (kb->size == 0)
// 			{
// 				kb->first = NULL;
// 				kb->last = NULL;
// 			}

//             return true;
// 		}

// 		curr = curr->next;
// 	}

//     return false;
// }

// FactLinkedList* matchFactKB(KnowledgeBase* kb, Fact* pattern)
// {
// 	/*
//     * For each fact, check if it matches with the pattern.
//     * A match can occur if b fact checks all litteral parts of the pattern,
//     * or if the pattern has variables and all the instances of the variables are checked.
//     */

//     if (verboseOn)
//     {
//         printf("Enter function matchFact.\n");
//     }

// 	FactLinkedList* res = createFactLinkedList();

//     VariablesPositions* str = (VariablesPositions*)getVariablesPositions(pattern);

//     if (strcmp(pattern->predicate, ">?") == 0
//         || strcmp(pattern->predicate, ">=?") == 0
//         || strcmp(pattern->predicate, "<?") == 0
//         || strcmp(pattern->predicate, "<=?") == 0)
//     {
//         LinkedList* variables = createLinkedList();
//         LinkedList* values = createLinkedList();

//         KnowledgeBaseNode* currFact = kb->first;
//         while (currFact != NULL)
//         {
//             Fact* f = (Fact*)currFact->data;
//             if (!LinkedListContainsString(variables, f->subject))
//             {
//                 pushLinkedList(variables, f->subject);
//             }
//             if (!LinkedListContainsString(variables, f->object))
//             {
//                 pushLinkedList(variables, f->object);
//             }
//             currFact = currFact->next;
//         }

//         LinkedListNode* currVar = variables->first;
//         while (currVar != NULL)
//         {
//             char* var = (char*)currVar->data;
//             char* value = getVariableValueKB(var, kb);
//             pushLinkedList(values, value);
//             currVar = currVar->next;
//         }

//         currVar = (LinkedListNode*)variables->first;
//         LinkedListNode* currVal = (LinkedListNode*)values->first;
//         while (currVar != NULL)
//         {
//             LinkedListNode* currOtherVar = (LinkedListNode*)variables->first;
//             LinkedListNode* currOtherVal = (LinkedListNode*)values->first;

//             while (currOtherVar != NULL)
//             {
//                 char* var = (char*)currVar->data;
//                 char* otherVar = (char*)currOtherVar->data;
//                 char* val = (char*)currVal->data;
//                 char* otherVal = (char*)currOtherVal->data;

//                 if (val != NULL && otherVal != NULL && strcmp(var, otherVar) != 0)
//                 {
//                     double numericVal = getDoubleFromTerm(val);
//                     double numericOtherVal = getDoubleFromTerm(otherVal);

//                     bool shouldAddFact = false;

//                     if (strcmp(pattern->predicate, ">?") == 0)
//                     {
//                         if (numericVal > numericOtherVal)
//                         {
//                             shouldAddFact = true;
//                         }
//                     }
//                     else if (strcmp(pattern->predicate, ">=?") == 0)
//                     {
//                         if (numericVal >= numericOtherVal)
//                         {
//                             shouldAddFact = true;
//                         }
//                     }
//                     else if (strcmp(pattern->predicate, "<?") == 0)
//                     {
//                         if (numericVal < numericOtherVal)
//                         {
//                             shouldAddFact = true;
//                         }
//                     }else if (strcmp(pattern->predicate, "<=?") == 0)
//                     {
//                         if (numericVal <= numericOtherVal)
//                         {
//                             shouldAddFact = true;
//                         }
//                     }

//                     if (shouldAddFact)
//                     {
//                         if (str->subject || (!str->subject && strcmp(var, pattern->subject) == 0))
//                         {
//                             if (str->object || (!str->object && strcmp(otherVar, pattern->object) == 0))
//                             {
//                                 Fact* newFact = createFact(var, pattern->predicate, otherVar, false);
//                                 if (!hasFact(res, newFact))
//                                 {
//                                     addFact(res, newFact);
//                                 }
                                
//                                 else
//                                 {
//                                     destroyFact(newFact, false);
//                                 }
//                             }
//                         }
//                     }                    
//                 }

//                 currOtherVar = currOtherVar->next;
//                 currOtherVal = currOtherVal->next;
//             }

//             currVar = currVar->next;
//             currVal = currVal->next;
//         }

//         freeLinkedList(variables);
//         freeLinkedList(values);
//     }
//     else
//     {
//         LinkedListNode* currFact = (LinkedListNode*)kb->first;
//         while (currFact != NULL)
//         {
//             bool match = true;
//             Fact* f = (Fact*)currFact->data;
//             Fact* atom_copy = copyFact(pattern, false);

//             if (atom_copy)
//             {
//                 // Check subject
//                 if (strcmp(atom_copy->subject, f->subject) == 0 || str->subject)
//                 {
//                     atom_copy->subject = f->subject;
//                 }
//                 else
//                 {
//                     match = false;
//                 }

//                 // Check predicate
//                 if (match && (strcmp(atom_copy->predicate, f->predicate) == 0 || str->predicate))
//                 {
//                     if (str->predicate)
//                     {
//                         if (strcmp(pattern->predicate, pattern->subject) == 0)
//                         {
//                             if (strcmp(f->predicate, atom_copy->subject) == 0)
//                             {
//                                 atom_copy->predicate = f->predicate;
//                             }
//                             else
//                             {
//                                 match = false;
//                             }
//                         }
//                         else
//                         {
//                             atom_copy->predicate = f->predicate;
//                         }
//                     }
//                     else 
//                     {
//                         atom_copy->predicate = f->predicate;
//                     }
//                 }
//                 else
//                 {
//                     match = false;
//                 }

//                 // Check object
//                 if (match && (strcmp(atom_copy->object, f->object) == 0 || str->object))
//                 {
//                     if (str->object)
//                     {
//                         if (strcmp(pattern->object, pattern->subject) == 0)
//                         {
//                             if (strcmp(f->object, atom_copy->subject) != 0)
//                             {
//                                 match = false;
//                             }
//                         }

//                         if (strcmp(pattern->object, pattern->predicate) == 0)
//                         {
//                             if (strcmp(f->object, atom_copy->predicate) != 0)
//                             {
//                                 match = false;
//                             }
//                         }
//                     }
//                 }
//                 else
//                 {
//                     match = false;
//                 }

//                 if (match)
//                 {
//                     atom_copy->object = f->object;
//                 }

//                 if (match)
//                 {
//                     if (verboseOn)
//                     {
//                         printf("Match found between fact (%s %s %s) and condition (%s %s %s).\n", f->subject, f->predicate, f->object,
//                                                                                                 pattern->subject, pattern->predicate, pattern->object);
//                     }
//                     if (!hasFact(res, f))
//                     {
//                         addFact(res, f);
//                     }
//                 }

//                 destroyFact(atom_copy, false);
//             }

//             currFact = currFact->next;
//         }
//     }

//     free(str);

//     if (verboseOn)
//     {
//         printf("Leave function checkAlphaNode.\n");
//     }

// 	return res;
// }