#include "Verbose.h"

// ---------------------------------------------- Constants ----------------------------------------------------

const char* VERBOSE_ARGS[] = {"-v", "--verbose"};
const int NB_VERBOSE_ARGS = 2;

// ---------------------------------------------- Variables ----------------------------------------------------

bool verboseOn = false;

// ---------------------------------------------- Functions ----------------------------------------------------

void detectVerbose(int argc, char** argv)
{
    for (int i = 0; i < argc; i++)
    {
        for (int j = 0; j < NB_VERBOSE_ARGS; j++)
        {
            if (strcmp(argv[i], VERBOSE_ARGS[j]) == 0)
            {
                verboseOn = true;
            }
        }
    }
}