#include "Rule.h"

#ifndef ALPHANODE_H_INCLUDED
#define ALPHANODE_H_INCLUDED

/**
 * Alpha node in a Rete network.
 * 
 * Each alpha node is associated to an atomic rule condition.
 */
typedef struct AlphaNode
{
    /**
     * Reference to the following beta node in the network.
     */
    struct BetaNode* successor;

    /**
     * Reference to the atomic condition to which the alpha node is associated.
     **/
    struct Fact* atom;

    /**
     * Reference to the rule to which the alpha node is associated.
     */
    struct Rule* rule;

    /**
     * Facts that match the atomic condition of the alpha node.
     * Actual type: LinkedList<Fact*>
     */
    struct FactLinkedList* matches; // LinkedList<Fact*>

    struct FactLinkedList* newMatches;

    struct FactLinkedList* deletedMatches;

    bool shared;
    LinkedList* sharedAlphaNodes;
    bool triggered;
    bool checked;
    size_t oldNewMatchesSize;
    size_t oldDeletedMatchesSize;

} AlphaNode;

/**
 * @relates AlphaNode
 * Creates an alpha node.
 * 
 * @param atom atomic condition associated to the node
 * @param rule rule associated to atom
 */
AlphaNode* createAlphaNode(Fact* atom, Rule* rule);

/**
 * @relates AlphaNode
 * Runs through all the facts and checks which ones match with the alpha node's condition.
 * @param alpha the alpha node to be tested
 * @param kb facts to be matched with the node
 */
bool checkAlphaNode(void* rete, AlphaNode* alpha, SordModel* kb, Term* insertTermFunction(void*, void*, bool));

/**
 * @relates AlphaNode
 * Returns the litteral value of a resource.
 * Uses the rdf:value predicate.
 * 
 * Example : 
 * If the knowledge base contains the following statement: ex:measurement rdf:value "20.0"^^xsd:double
 * 
 * Then the function call \code{.c} getResourceValue("http://example.org#measurement", kb) \endcode returns the string "20.0".
 * 
 * @return The string value of the resource, NULL otherwise.
 * 
 * @warning The knowledge base should not contain more than one statement indicating the value of a resource. However if this condition is not respected, the function will return the first matching statement in the knowledge base.
 */
char* getResourceValue(char* resource, SordModel* kb);

void moveAlphaMatches(AlphaNode* alpha);

bool checkAlphaNodeNewFact(AlphaNode* alpha, Fact* f);

bool checkAlphaNodeRemoveFacts(AlphaNode* alpha, SordModel* kb);

bool alphaNodeMatchFact(AlphaNode* alpha, Fact* f);

#endif // !ALPHANODE_H_INCLUDED