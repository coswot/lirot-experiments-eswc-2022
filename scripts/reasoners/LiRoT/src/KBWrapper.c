#include "KBWrapper.h"

char* APIReadFile(char* filepath)
{
    char* out = 0;
    long length;
    FILE * f = fopen (filepath, "rb");

    if (f)
    {
        fseek (f, 0, SEEK_END);
        length = ftell (f);
        fseek (f, 0, SEEK_SET);
        out = (char*)malloc (length + 1);
        if (out)
        {
            fread (out, length, 1, f);
        }
        fclose (f);
    }

    return out;
}

KBWrapper* APIInitKBWrapper(char* initialFactFilePath, char* rulesFilePath)
{
    KBWrapper* w = (KBWrapper*)malloc(sizeof(KBWrapper));
    if (w)
    {
        SordWorld* world = sord_world_new();
        SordModel* kb = sord_new(world, SORD_SPO, false);

        LinkedList* rulesLines = getLines(rulesFilePath);
        LinkedList* rules = getRules(rulesLines);

        // Free lines
        LinkedListNode* currLine = rulesLines->first;
        while (currLine != NULL)
        {
            free((char*)currLine->data);
            currLine = currLine->next;
        }
        freeLinkedList(rulesLines);

        w->kb = kb;
        w->rete = createReteNetwork(rules, kb);

        if (initialFactFilePath != NULL)
        {
            SerdEnv* env = serd_env_new(NULL);
            SerdReader* reader = sord_new_reader(kb, env, SERD_TURTLE, NULL);

            FILE* file = fopen(initialFactFilePath, "r");
            SerdStatus status = serd_reader_read_file_handle(reader, file, NULL);
            if (status == SERD_SUCCESS)
            {
                fclose(file);
                serd_env_free(env);
                serd_reader_free(reader);
                // printf("%zu facts before inference.\n", sord_num_quads(kb));
                addExplicitFacts(w->rete, NULL);
            }
            else
            {
                fclose(file);
                serd_env_free(env);
                serd_reader_free(reader);
            }
        }

        
    }

    return w;
}

void APIDestroyKBWrapper (KBWrapper* w)
{
    destroyRete(w->rete);
    SordWorld* world = sord_get_world(w->kb);
    sord_free(w->kb);
    sord_world_free(world);
    free(w);
}

// // --------------------------- API -----------------------------

// bool APIAddFact(KBWrapper* w, char* s, char* p, char* o, char* g)
// {
//     SordWorld* world = sord_get_world(w->kb);
//     SordNode* subject = sord_new_uri(world, (const uint8_t*)s);
//     SordNode* predicate = sord_new_uri(world, (const uint8_t*)p);
//     SordNode* object = sord_new_uri(world, (const uint8_t*)o);
//     SordNode* graph = 0;
//     SordQuad quad = {subject, predicate, object, graph};

//     bool result = sord_add(w->kb, quad);
//     FactLinkedList* newFacts = APIInfer(w);

//     FactLinkedListNode* currFact = newFacts->first;
//     while (currFact != NULL)
//     {
//         Fact* f = (Fact*)currFact->data;
//         SordNode* newSubject = sord_new_uri(world, (const uint8_t*)f->subject);
//         SordNode* newPredicate = sord_new_uri(world, (const uint8_t*)f->predicate);
//         SordNode* newObject = sord_new_uri(world, (const uint8_t*)f->object);
//         SordNode* newGraph = 0;

//         if (!sord_ask(w->kb, newSubject, newPredicate, newObject, NULL))
//         {
//             SordQuad newQuad = {newSubject, newPredicate, newObject, newGraph};
//             sord_add(w->kb, newQuad);
//         }

//         destroyFact(f, false);

//         currFact = currFact->next;
//     }

//     freeFactLinkedList(newFacts);

//     return result;
// }

bool APIAddFactsFromText(KBWrapper* w, char* text)
{
    SordWorld* world = sord_get_world(w->kb);
    SerdEnv* env = serd_env_new(NULL);
    SordModel* tempKB = sord_new(world, SORD_SPO, false);
    SerdReader* reader = sord_new_reader(tempKB, env, SERD_TURTLE, NULL);

    bool result;

    SerdStatus resParse = serd_reader_read_string(reader, (uint8_t*)text);
    if (resParse == SERD_SUCCESS)
    {
        free(text);
        serd_reader_free(reader);
        serd_env_free(env);
        // printf("%zu facts to add.\n", sord_num_quads(tempKB));

        SordIter* iter;
        for (iter = sord_begin(tempKB); !sord_iter_end(iter);)
        {
            SordQuad quad;
            sord_iter_get(iter, quad);
            bool addedFact = sord_add(w->kb, quad);
            if (!addedFact)
            {
                sord_erase(tempKB, iter);
            }
            else
            {
                sord_iter_next(iter);
            }
        }
        addExplicitFacts(w->rete, tempKB);
        result = true;
    }
    else
    {
        result = false;
        free(text);
        serd_reader_free(reader);
        serd_env_free(env);
    }

    sord_free(tempKB);

    return result;
}

bool APIAddFactsFromFile(KBWrapper* w, char* filename)
{
    bool result = false;

    FILE* file = fopen(filename, "r");
    if (file)
    {
        SordWorld* world = sord_get_world(w->kb);
        SerdEnv* env = serd_env_new(NULL);
        SordModel* tempKB = sord_new(world, SORD_SPO, false);
        SerdReader* reader = sord_new_reader(tempKB, env, SERD_TURTLE, NULL);

        SerdStatus resParse = serd_reader_read_file_handle(reader, file, NULL);
        if (resParse == SERD_SUCCESS)
        {
            fclose(file);
            serd_reader_free(reader);
            serd_env_free(env);
            // printf("%zu facts to add.\n", sord_num_quads(tempKB));

            SordIter* iter;
            for (iter = sord_begin(tempKB); !sord_iter_end(iter);)
            {
                SordQuad quad;
                sord_iter_get(iter, quad);
                bool addedFact = sord_add(w->kb, quad);
                if (!addedFact)
                {
                    sord_erase(tempKB, iter);
                }
                else
                {
                    sord_iter_next(iter);
                }
            }
            addExplicitFacts(w->rete, tempKB);
            result = true;
        }
        else
        {
            result = false;
            fclose(file);
            serd_reader_free(reader);
            serd_env_free(env);
        }

        sord_free(tempKB);
    }
    else
    {
        fclose(file);
    }

    return result;
}

// bool APIHasFact(KBWrapper* w, char* s, char* p, char* o, char* g)
// {
//     SordWorld* world = sord_get_world(w->kb);
//     SordNode* subject = sord_new_uri(world, (const uint8_t*)s);
//     SordNode* predicate = sord_new_uri(world, (const uint8_t*)p);
//     SordNode* object = sord_new_uri(world, (const uint8_t*)o);
//     SordNode* graph = 0;

//     bool result = sord_ask(w->kb, subject, predicate, object, graph);

//     sord_node_free(world, subject);
//     sord_node_free(world, predicate);
//     sord_node_free(world, object);
//     sord_node_free(world, graph);

//     return result;
// }

// void APIDeleteFact(KBWrapper* w, char* s, char* p, char* o, char* g)
// {
//     SordWorld* world = sord_get_world(w->kb);
//     SordNode* subject = sord_new_uri(world, (const uint8_t*)s);
//     SordNode* predicate = sord_new_uri(world, (const uint8_t*)p);
//     SordNode* object = sord_new_uri(world, (const uint8_t*)o);
//     SordNode* graph = 0;
//     SordQuad quad = {subject, predicate, object, graph};

//     sord_remove(w->kb, quad);

//     sord_node_free(world, subject);
//     sord_node_free(world, predicate);
//     sord_node_free(world, object);
//     sord_node_free(world, graph);
// }

bool APIDeleteFactsFromText(KBWrapper* w, char* text)
{
    SordWorld* world = sord_get_world(w->kb);
    SerdEnv* env = serd_env_new(NULL);
    SordModel* tempKB = sord_new(world, SORD_SPO, false);
    SerdReader* reader = sord_new_reader(tempKB, env, SERD_TURTLE, NULL);

    bool result;

    SerdStatus resParse = serd_reader_read_string(reader, (uint8_t*)text);
    if (resParse == SERD_SUCCESS)
    {
        free(text);
        serd_env_free(env);
        serd_reader_free(reader);
        // printf("%zu facts to delete\n", sord_num_quads(tempKB));

        SordIter* tempFacts = sord_begin(tempKB);
        int count = 0;
        while (!sord_iter_end(tempFacts))
        {
            SordQuad quad;
            sord_iter_get(tempFacts, quad);
            sord_remove(w->kb, quad);
            
            sord_iter_next(tempFacts);
            count++;
        }
        sord_iter_free(tempFacts);
        // printf("%d explicit facts deleted\n", count);

        removeExplicitFacts(w->rete, tempKB);

        result = true;
    }
    else
    {
        free(text);
        serd_env_free(env);
        serd_reader_free(reader);
        result = false;
    }

    sord_free(tempKB);

    return result;
}

bool APIDeleteFactsFromFile(KBWrapper* w, char* filename)
{
    bool result = false;

    FILE* file = fopen(filename, "r");
    if (file)
    {
        SordWorld* world = sord_get_world(w->kb);
        SerdEnv* env = serd_env_new(NULL);
        SordModel* tempKB = sord_new(world, SORD_SPO, false);
        SerdReader* reader = sord_new_reader(tempKB, env, SERD_TURTLE, NULL);

        SerdStatus resParse = serd_reader_read_file_handle(reader, file, NULL);
        if (resParse == SERD_SUCCESS)
        {
            fclose(file);
            serd_env_free(env);
            serd_reader_free(reader);
            // printf("%zu facts to delete\n", sord_num_quads(tempKB));

            SordIter* tempFacts = sord_begin(tempKB);
            int count = 0;
            while (!sord_iter_end(tempFacts))
            {
                SordQuad quad;
                sord_iter_get(tempFacts, quad);
                sord_remove(w->kb, quad);
                
                sord_iter_next(tempFacts);
                count++;
            }
            sord_iter_free(tempFacts);
            // printf("%d explicit facts deleted\n", count);

            removeExplicitFacts(w->rete, tempKB);

            result = true;
        }
        else
        {
            fclose(file);
            serd_env_free(env);
            serd_reader_free(reader);
            result = false;
        }

        //sord_free(tempKB);
    }
    else
    {
        fclose(file);
    }

    return result;
}

char* APIMatchFact(KBWrapper* w, char* s, char* p, char* o, char* g)
{
    SordWorld* world = sord_get_world(w->kb);
    SordNode* subject = s != NULL ? sord_new_uri(world, (const uint8_t*)s) : NULL;
    SordNode* predicate = p != NULL ? sord_new_uri(world, (const uint8_t*)p) : NULL;
    SordNode* object = o != NULL ? sord_new_uri(world, (const uint8_t*)o) : NULL;
    SordNode* graph = g != NULL ? sord_new_uri(world, (const uint8_t*)g) : NULL;
    SordQuad pattern = {subject, predicate, object, graph};

    SerdEnv* env = serd_env_new(NULL);

    SerdChunk chunk = {NULL, 0};
    SerdWriter* writer = serd_writer_new(SERD_TURTLE, SERD_STYLE_ABBREVIATED, env, NULL, serd_chunk_sink, &chunk);

    SordIter* res = sord_find(w->kb, pattern);

    sord_node_free(world, subject);
    sord_node_free(world, predicate);
    sord_node_free(world, object);
    sord_node_free(world, graph);

    while (!sord_iter_end(res))
    {
        SordQuad quad;
        sord_iter_get(res, quad);
        serd_writer_write_statement(writer, 0, quad[3], quad[0], quad[1], quad[2], NULL, NULL);
        sord_iter_next(res);
    }

    serd_writer_free(writer);
    serd_env_free(env);

    char* out = (char*)serd_chunk_sink_finish(&chunk);

    sord_iter_free(res);

    return out;
}
