#include "LinkedList.h"

#ifndef BETAVARIABLES_H_INCLUDED
#define BETAVARIABLES_H_INCLUDED

/**
 * Structure containing all the variables used by a beta node.
 * 
 * Example: Node 10 on example figure.
 * 
 * \code{.c}
 * BetaVariables
 * {
 *   joinVariables: LinkedList<char*>{"?x", "?w"};
 *   otherVariables: LinkedList<char*>{"?v", "?p", "?u"}
 * }
 * \endcode
 */
typedef struct BetaVariables
{
    /**
     * Variables used to join two alpha nodes or an alpha and a beta node.
     * Actual type: LinkedList<char*>
     */
    LinkedList* joinVariables;

    /**
     * Variables that are not used in the join operation for this specific node, but can be used later in the network.
     * Actual type: LinkedList<char*>
     */
    LinkedList* otherVariables;
} BetaVariables;

#endif // !BETAVARIABLES_H_INCLUDED