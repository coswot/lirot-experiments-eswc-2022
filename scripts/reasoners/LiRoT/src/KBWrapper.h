#include "Rete.h"
#include "serd/serd.h"
#include "sord/sord.h"

#ifndef REASONER_H_INCLUDED
#define REASONER_H_INCLUDED

/**
 * Structure representing the API of the reasoner.
 * 
 * The API is based on rdfjs.
 * 
 * The API can read and write text in Turtle, N-Triples, N-Quads and TriG formats.
 * \todo Either change the name of the struct or give a better description.
 */
typedef struct KBWrapper {

    /**
     * Knowledge base used by the reasoner.
     * Currently using the sord library.
     * Also launches inference after initilizing the knowledge base.
     */
    SordModel* kb;

    /**
     * The Rete implementation used for reasoning.
     */
    Rete* rete;
} KBWrapper;


/**
 * \relates KBWrapper
 * Creates and returns a new API.
 * Also creates a new knowledge base.
 * @param initialFactFilePath Path to a file with explicit facts to initialize the knowledge base.
 * @param rulesFilePath Path to a file with the ruleset used by the reasoner. Currently using a custom format (see readme for more information).
 */
KBWrapper* APIInitKBWrapper(char* initialFactFilePath, char* rulesFilePath);

/**
 * \relates KBWrapper
 * Launches inference.
 */
void APIInfer(KBWrapper* r);

/**
 * \relates KBWrapper
 * Frees dynamic space allocated to the API.
 */
void APIDestroyKBWrapper (KBWrapper* r);

// --------------------------- API -----------------------------

/**
 * \relates KBWrapper
 * Adds a new fact to the knowledge base, and launches inference.
 * @return True if the fact was added, false otherwise.
 * \todo Adapt behavior regarding the graph according to the format (triplets or quads).
 */
bool APIAddFact(KBWrapper* w, char* subject, char* predicate, char* object, char* graph);

/**
 * \relates KBWrapper
 * Bulk insertion of facts in the knowledge base (see input formats in the description of the API).
 * 
 * Also launches inference after insertion.
 * 
 * @return True if all facts could be added to the knowledge base, false otherwise.
 */
bool APIAddFactsFromText(KBWrapper* w, char* text);

bool APIAddFactsFromFile(KBWrapper* w, char* filename);

/**
 * \relates KBWrapper
 * Searches for a fact in the knowledge base.
 * @return True if the fact exists in the knowledge base, false otherwise.
 */
bool APIHasFact(KBWrapper* w, char* subject, char* predicate, char* object, char* graph);

/**
 * \relates KBWrapper
 * Removes a fact from the knowledge base, and launches inference.
 * @return True if the fact was removed, false otherwise.
 */
void APIDeleteFact(KBWrapper* w, char* subject, char* predicate, char* object, char* graph);

/**
 * \relates KBWrapper
 * Bulk deletion of facts in the knowledge base (see input formats in the description of the API).
 * Also launches inference after deletion.
 * 
 * @return True if all facts could be removed from the knowledge base, false otherwise.
 */
bool APIDeleteFactsFromText(KBWrapper* w, char* text);

bool APIDeleteFactsFromFile(KBWrapper* w, char* filename);

/**
 * \relates KBWrapper
 * Finds if facts in the knowledge base match the pattern provided as input.
 * Every variable in the pattern must be set to NULL.
 * 
 * \warning This function does not support the case where one same variable is used multiple times in the pattern (e.g. (NULL, rdfs:subClassOf, NULL, ex:graph1); in this case the function will also return all facts with distinct subject and object).
 * @return All facts matching the pattern in Turtle format.
 * \todo Either add return format as an input or used one defined by the user when the API is initialized.
 */
char* APIMatchFact(KBWrapper* w, char* subject, char* predicate, char* object, char* graph);

char* APIReadFile(char* filepath);

#endif // !REASONER_H_INCLUDED
