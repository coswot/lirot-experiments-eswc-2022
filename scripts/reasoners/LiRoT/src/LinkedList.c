#include "LinkedList.h"

// ----------------------------------- Linked List -------------------------------------

LinkedList* createLinkedList()
{
	LinkedList* l = (LinkedList*)malloc(sizeof(LinkedList));
	if (l)
	{
		l->size = 0;
		l->first = NULL;
		l->last = NULL;
	}
	

	return l;
}

void pushLinkedList(LinkedList* l, void* element)
{
	LinkedListNode* newElem = (LinkedListNode*)malloc(sizeof(LinkedListNode));
	if (newElem)
	{

		newElem->next = NULL;
		newElem->data = element;
		if (l->size == 0)
		{
			l->first = newElem;
			l->last = l->first;
		}
		else
		{
			l->last->next = newElem;
			l->last = newElem;
		}
	}
	
	l->size++;
}

LinkedListNode* deleteLinkedListCursor(LinkedList* l, LinkedListNode* cursor)
{
	LinkedListNode* next = NULL;

	if (cursor != l->last)
	{
		if (cursor == l->first)
		{
			l->first = l->first->next;
			next = l->first;
			free(cursor);
			l->size--;
		}
		else
		{
			LinkedListNode* prec = l->first;
			while (prec->next != cursor)
			{
				prec = prec->next;
			}
			prec->next = prec->next->next;
			next = prec->next;
			free(cursor);
			l->size--;
		}
	}
	else
	{
		if (l->size == 1)
		{
			free(cursor);
			l->first = NULL;
			l->last = NULL;
			l->size = 0;
		}
		else
		{
			LinkedListNode* prec = l->first;
			while (prec->next != cursor)
			{
				prec = prec->next;
			}
			prec->next = NULL;
			l->last = prec;
			free(cursor);
			l->size--;
		}
	}

	if (l->size == 1)
	{
		l->last = l->first;
	}

	if (l->size == 0)
	{
		l->first = NULL;
		l->last = NULL;
	}

	return next;
}

void insertLinkedListAtIndex(LinkedList* l, void* element, int index)
{
	if (index == 0)
	{
		LinkedListNode* newElem = (LinkedListNode*)malloc(sizeof(LinkedListNode));
		if (newElem)
		{
			newElem->data = element;
			newElem->next = l->first;
			l->first = newElem;
			l->size++;
		}
	}
	else if (index > 0)
	{
		if (index == l->size)
		{
			pushLinkedList(l, element);
		}
		else if (index < l->size)
		{
			int i = 0;
			LinkedListNode* current = l->first;
			while (i < index - 1)
			{
				current = current->next;
				i++;
			}
			LinkedListNode* newElem = (LinkedListNode*)malloc(sizeof(LinkedListNode));
			if (newElem)
			{
				newElem->data = element;
				newElem->next = current->next;
				current->next = newElem;
				l->size++;
			}
		}
	}
}

bool LinkedListContains(LinkedList* l, void* element)
{
	LinkedListNode* curr = l->first;
	while (curr != NULL)
	{
		if (curr->data == element)
		{
			return true;
		}
		curr = curr->next;
	}

	return false;
}

bool LinkedListContainsString(LinkedList* l, char* element)
{
	LinkedListNode* curr = l->first;
	while (curr != NULL)
	{
		if(strcmp((char*)curr->data, element) == 0)
		{
			return true;
		}
		curr = curr->next;
	}

	return false;
}

void* getLinkedListAtIndex(LinkedList* l, int index)
{
	void* res = NULL;

	if (index >= 0 && index < l->size)
	{
		LinkedListNode* curr = l->first;
		int i = 0;
		while (i < index)
		{
			curr = curr->next;
			i++;
		}

		res = curr->data;
	}

	return res;
}

void removeLinkedListAtIndex(LinkedList* l, int index)
{
	if (index < l->size)
	{
		if (index == 0)
		{
			LinkedListNode* toRemove = l->first;
			l->first = l->first->next;
			free(toRemove);
			l->size--;
		}
		else
		{
			LinkedListNode* current = l->first;
			int i = 0;
			while (i < index - 1)
			{
				current = current->next;
				i++;
			}
			LinkedListNode* toRemove = current->next;
			current->next = current->next->next;
			free(toRemove);
			l->size--;
		}
	}

	if (l->size == 1)
	{
		l->last = l->first;
	}

	if (l->size == 0)
	{
		l->first = NULL;
		l->last = NULL;
	}
}

void freeLinkedList(LinkedList* l)
{
	LinkedListNode* current = l->first;
	LinkedListNode* tmp;

	while (current != NULL)
	{
		tmp = current;
		current = current->next;
		free(tmp);
	}
	
	free(l);
}