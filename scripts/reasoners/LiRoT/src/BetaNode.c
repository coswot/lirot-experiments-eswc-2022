#include "BetaNode.h"

BetaNode* createBetaNodeFromAlpha(Rule* rule, AlphaNode* leftNode, AlphaNode* rightNode)
{
    BetaNode* result = (BetaNode*)malloc(sizeof(BetaNode));
    if (result)
    {
        result->terminalNode = true;
        result->rule = rule;
        leftNode->successor = result;
        if (rightNode != NULL)
        {
            rightNode->successor = result;
        }

        result->successor = NULL;

        result->alphaLeftPredecessor = leftNode;
        result->rightPredecessor = rightNode;
        result->fromAlpha = true;
        result->matches = createLinkedList();
        result->newMatches = createLinkedList();
        result->deletedMatches = createLinkedList();
        result->matchingAlphaNodes = NULL;

        result->variables = getCommonVariablesAlpha(leftNode, rightNode);

        result->implicitFacts = NULL;
    }

    if (verboseOn)
    {
        printf("Created BetaNode.\n");
    }
    
    return result;
}

BetaNode* createBetaNodeFromBeta(Rule* rule, BetaNode* leftNode, AlphaNode* rightNode)
{
    BetaNode* result = (BetaNode*)malloc(sizeof(BetaNode));
    if (result)
    {
        result->terminalNode = true;
        result->rule = rule;
        leftNode->successor = result;
        leftNode->terminalNode = false;
        rightNode->successor = result;

        result->betaLeftPredecessor = leftNode;
        result->rightPredecessor = rightNode;
        result->fromAlpha = false;
        result->matches = createLinkedList();
        result->matchingAlphaNodes = NULL;

        result->variables = getCommonVariablesBeta(leftNode, rightNode);

        result->implicitFacts = NULL;
    }

    if (verboseOn)
    {
        printf("Created BetaNode.\n");
    }
    
    return result;
}

BetaVariables* getCommonVariablesAlpha(AlphaNode* left, AlphaNode* right)
{
    if (verboseOn)
    {
        printf("Enter function getCommonVariablesAlpha.\n");
    }

    BetaVariables* result = (BetaVariables*)malloc(sizeof(BetaVariables));
    if (result)
    {
        // Left predecessor
        LinkedList* leftVars = createLinkedList();

        // Check all variables in the left predecessor
        if (left->atom->subject->type == VARIABLE)
        {
            pushLinkedList(leftVars, left->atom->subject->value);
        }
        if (left->atom->predicate->type == VARIABLE)
        {
            pushLinkedList(leftVars, left->atom->predicate->value);
        }
        if (left->atom->object->type == VARIABLE)
        {
            pushLinkedList(leftVars, left->atom->object->value);
        }

        // Right predecessor
        if (right != NULL)
        {
            LinkedList* rightVars = createLinkedList();

            // Check all variables in the right predecessor
            if (right->atom->subject->type == VARIABLE)
            {
                pushLinkedList(rightVars, right->atom->subject->value);
            }
            if (right->atom->predicate->type == VARIABLE)
            {
                pushLinkedList(rightVars, right->atom->predicate->value);
            }
            if (right->atom->object->type == VARIABLE)
            {
                pushLinkedList(rightVars, right->atom->object->value);
            }

            // Check variables in common between the two predecessors
            LinkedList* joinVars = createLinkedList();
            LinkedListNode* currLeftVar = leftVars->first;
            while (currLeftVar != NULL)
            {
                char* leftV = (char*)currLeftVar->data;
                LinkedListNode* currRightVar = rightVars->first;
                while (currRightVar != NULL)
                {
                    char* rightV = (char*) currRightVar->data;
                    if (strcmp(leftV, rightV) == 0 && !LinkedListContainsString(joinVars, leftV))
                    {
                        pushLinkedList(joinVars, leftV);
                    }

                    currRightVar = currRightVar->next;
                }

                currLeftVar = currLeftVar->next;
            }

            // Place the other variables in b separate array
            LinkedList* otherVars = createLinkedList();
            currLeftVar = leftVars->first;
            while (currLeftVar != NULL)
            {
                char* leftV = (char*)currLeftVar->data;

                if (!LinkedListContainsString(joinVars, leftV) && !LinkedListContainsString(otherVars, leftV))
                {
                    pushLinkedList(otherVars, leftV);
                }

                currLeftVar = currLeftVar->next;
            }

            LinkedListNode* currRightVar = rightVars->first;
            while (currRightVar != NULL)
            {
                char* rightV = (char*)currRightVar->data;
                if (!LinkedListContainsString(joinVars, rightV) && !LinkedListContainsString(otherVars, rightV))
                {
                    pushLinkedList(otherVars, rightV);
                }

                currRightVar = currRightVar->next;
            }

            result->joinVariables = joinVars;
            result->otherVariables = otherVars;

            freeLinkedList(leftVars);
            freeLinkedList(rightVars);
        }
        else {
            result->joinVariables = leftVars;
            result->otherVariables = createLinkedList();
        }
    }

    if (verboseOn)
    {
        printf("Found join variables { ");

        LinkedListNode* currV = result->joinVariables->first;
        while (currV != NULL)
        {
            printf("%s ", (char*)currV->data);
            currV = currV->next;
        }

        printf ("} and other variables { ");
        currV = result->otherVariables->first;
        while (currV != NULL)
        {
            printf("%s ", (char*)currV->data);
            currV = currV->next;
        }

        printf("}.\nLeave function getCommonVariablesAlpha.\n");
    }
    
    return result;
}

BetaVariables* getCommonVariablesBeta(BetaNode* left, AlphaNode* right)
{
    if (verboseOn)
    {
        printf("Enter function getCommonVariablesBeta.\n");
    }

    BetaVariables* result = (BetaVariables*)malloc(sizeof(BetaVariables));
    if (result)
    {
        LinkedList* rightVars = createLinkedList();

        // Check all variables in the right predecessor
        if (right->atom->subject->type == VARIABLE)
        {
            pushLinkedList(rightVars, right->atom->subject->value);
        }
        if (right->atom->predicate->type == VARIABLE)
        {
            pushLinkedList(rightVars, right->atom->predicate->value);
        }
        if (right->atom->object->type == VARIABLE)
        {
            pushLinkedList(rightVars, right->atom->object->value);
        }

        // Find current node's join variables
        // Check variables in common between the right predecessor and the left beta predecessor's join variables
        LinkedList* joinVars = createLinkedList();
        LinkedListNode* currLeftJoin = left->variables->joinVariables->first;
        while (currLeftJoin != NULL)
        {
            char* leftJ = (char*)currLeftJoin->data;
            LinkedListNode* currRightVar = rightVars->first;
            while (currRightVar != NULL)
            {
                char* rightV = (char*)currRightVar->data;
                if (strcmp(leftJ, rightV) == 0 && !LinkedListContainsString(joinVars, leftJ))
                {
                    pushLinkedList(joinVars, leftJ);
                }

                currRightVar = currRightVar->next;
            }

            currLeftJoin = currLeftJoin->next;
        }

        // Check variables in common between the right predecessor and the left beta predecessor's other variables
        LinkedListNode* currLeftOther = left->variables->otherVariables->first;
        while (currLeftOther != NULL)
        {
            char* leftO = (char*)currLeftOther->data;
            LinkedListNode* currRightVar = rightVars->first;
            while (currRightVar != NULL)
            {
                char* rightV = (char*)currRightVar->data;

                if (strcmp(leftO, rightV) == 0 && !LinkedListContainsString(joinVars,  leftO))
                {
                    pushLinkedList(joinVars, leftO);
                }

                currRightVar = currRightVar->next;
            }

            currLeftOther = currLeftOther->next;
        }

        // Find current node's other variables
        // Check variables in common between the right predecessor and the left beta predecessor's join variables
        LinkedList* otherVars = createLinkedList();

        currLeftJoin = left->variables->joinVariables->first;
        while (currLeftJoin != NULL)
        {
            char* leftJ = (char*)currLeftJoin->data;
            if (!LinkedListContainsString(otherVars, leftJ) && !LinkedListContainsString(joinVars, leftJ))
            {
                pushLinkedList(otherVars, leftJ);
            }

            currLeftJoin = currLeftJoin->next;
        }

        currLeftOther = left->variables->otherVariables->first;
        while (currLeftOther != NULL)
        {
            char* leftO = (char*)currLeftOther->data;
            if (!LinkedListContainsString(otherVars, leftO) && !LinkedListContainsString(joinVars, leftO))
            {
                pushLinkedList(otherVars, leftO);
            }

            currLeftOther = currLeftOther->next;
        }

        // Check variables in common between the right predecessor and the left beta predecessor's other variables
        LinkedListNode* currRightVar = rightVars->first;
        while (currRightVar != NULL)
        {
            char* rightV = (char*)currRightVar->data;
            if (!LinkedListContainsString(joinVars, rightV) && !LinkedListContainsString(otherVars, rightV))
            {
                pushLinkedList(otherVars, rightV);
            }

            currRightVar = currRightVar->next;
        }

        result->joinVariables = joinVars;
        result->otherVariables = otherVars;

        freeLinkedList(rightVars);
    }

    if (verboseOn)
    {
        printf("Found join variables { ");

        LinkedListNode* currV = result->joinVariables->first;
        while (currV != NULL)
        {
            printf("%s ", (char*)currV->data);
            currV = currV->next;
        }

        printf ("} and other variables { ");
        currV = result->otherVariables->first;
        while (currV != NULL)
        {
            printf("%s ", (char*)currV->data);
            currV = currV->next;
        }

        printf("}.\nLeave function getCommonVariablesBeta.\n");
    }
    
    return result;
}

bool checkBetaNodeFromTwoAlphaNodes(BetaNode* beta, FactLinkedList* leftMatches, FactLinkedList* rightMatches, size_t leftLimit, size_t rightLimit, LinkedList* betaMatches)
{
    bool checkNext = false;

    // LinkedListNode* currLeftMatch = beta->alphaLeftPredecessor->matches->first;
    FactLinkedListNode* currLeftMatch = leftMatches->first;
    int countLeft = 0;
    while (currLeftMatch != NULL && countLeft < leftLimit)
    {
        countLeft++;
        Fact* leftMatch = (Fact*)currLeftMatch->data;
        //LinkedListNode* currRightMatch = beta->rightPredecessor->matches->first;
        FactLinkedListNode* currRightMatch = rightMatches->first;
        int countRight = 0;
        while (currRightMatch != NULL && countRight < rightLimit)
        {
            countRight++;
            Fact* rightMatch = (Fact*)currRightMatch->data;
            bool match = false;

            BetaMatch* m = createBetaMatch(beta->variables->joinVariables->size + beta->variables->otherVariables->size);

            //Convention: variables index follows the nbJoinVariables + nbOtherVariables pattern
            LinkedListNode* currJoinVar = beta->variables->joinVariables->first;
            int k = 0;
            while (currJoinVar != NULL)
            {
                char* joinV = (char*)currJoinVar->data;
                //Check left subject
                if (beta->alphaLeftPredecessor->atom->subject->type == VARIABLE && strcmp(beta->alphaLeftPredecessor->atom->subject->value, joinV) == 0)
                {
                    //Check right subject
                    if (beta->rightPredecessor->atom->subject->type == VARIABLE && strcmp(beta->rightPredecessor->atom->subject->value, joinV) == 0 && termEquals(leftMatch->subject, rightMatch->subject))
                    {
                        m->variables[k] = leftMatch->subject;
                        match = true;
                    }

                    //Check right predicate
                    if (beta->rightPredecessor->atom->predicate->type == VARIABLE && strcmp(beta->rightPredecessor->atom->predicate->value, joinV) == 0 && termEquals(leftMatch->subject, rightMatch->predicate))
                    {
                        m->variables[k] = leftMatch->subject;
                        match = true;
                    }

                    //Check right object
                    if (beta->rightPredecessor->atom->object->type == VARIABLE && strcmp(beta->rightPredecessor->atom->object->value, joinV) == 0 && termEquals(leftMatch->subject, rightMatch->object))
                    {
                        m->variables[k] = leftMatch->subject;
                        match = true;
                    }
                }

                //Check left predicate
                if (beta->alphaLeftPredecessor->atom->predicate->type == VARIABLE && strcmp(beta->alphaLeftPredecessor->atom->predicate->value, joinV) == 0)
                {
                    //Check right subject
                    if (beta->rightPredecessor->atom->subject->type == VARIABLE && strcmp(beta->rightPredecessor->atom->subject->value, joinV) == 0 && termEquals(leftMatch->predicate, rightMatch->subject))
                    {
                        m->variables[k] = leftMatch->predicate;
                        match = true;
                    }

                    //Check right predicate
                    if (beta->rightPredecessor->atom->predicate->type == VARIABLE && strcmp(beta->rightPredecessor->atom->predicate->value, joinV) == 0 && termEquals(leftMatch->predicate, rightMatch->predicate))
                    {
                        m->variables[k] = leftMatch->predicate;
                        match = true;
                    }

                    //Check right object
                    if (beta->rightPredecessor->atom->object->type == VARIABLE && strcmp(beta->rightPredecessor->atom->object->value, joinV) == 0 && termEquals(leftMatch->predicate, rightMatch->object))
                    {
                        m->variables[k] = leftMatch->predicate;
                        match = true;
                    }
                }

                //Check left object
                if (beta->alphaLeftPredecessor->atom->object->type == VARIABLE && strcmp(beta->alphaLeftPredecessor->atom->object->value, joinV) == 0)
                {
                    //Check right subject
                    if (beta->rightPredecessor->atom->subject->type == VARIABLE && strcmp(beta->rightPredecessor->atom->subject->value, joinV) == 0 && termEquals(leftMatch->object, rightMatch->subject))
                    {
                        m->variables[k] = leftMatch->object;
                        match = true;
                    }

                    //Check right predicate
                    if (beta->rightPredecessor->atom->predicate->type == VARIABLE && strcmp(beta->rightPredecessor->atom->predicate->value, joinV) == 0 && termEquals(leftMatch->object, rightMatch->predicate))
                    {
                        m->variables[k] = leftMatch->object;
                        match = true;
                    }

                    //Check right object
                    if (beta->rightPredecessor->atom->object->type == VARIABLE && strcmp(beta->rightPredecessor->atom->object->value, joinV) == 0 && termEquals(leftMatch->object, rightMatch->object))
                    {
                        m->variables[k] = leftMatch->object;
                        match = true;
                    }
                }

                currJoinVar = currJoinVar->next;
                k++;
            }

            // If b match is found, copy all the variables' values
            if (match)
            {
                if (verboseOn)
                {
                    printf("Found beta match between (%s %s %s) and (%s %s %s).\n",
                        leftMatch->subject->value, leftMatch->predicate->value, leftMatch->object->value,
                        rightMatch->subject->value, rightMatch->predicate->value, rightMatch->object->value);
                }

                LinkedListNode* currOtherVar = beta->variables->otherVariables->first;
                int k = 0;
                while (currOtherVar != NULL)
                {
                    char* otherV = (char*)currOtherVar->data;
                    if (beta->alphaLeftPredecessor->atom->subject->type == VARIABLE && strcmp(beta->alphaLeftPredecessor->atom->subject->value, otherV) == 0)
                    {
                        m->variables[beta->variables->joinVariables->size + k] = leftMatch->subject;
                    }

                    if (beta->alphaLeftPredecessor->atom->predicate->type == VARIABLE && strcmp(beta->alphaLeftPredecessor->atom->predicate->value, otherV) == 0)
                    {
                        m->variables[beta->variables->joinVariables->size + k] = leftMatch->predicate;
                    }

                    if (beta->alphaLeftPredecessor->atom->object->type == VARIABLE && strcmp(beta->alphaLeftPredecessor->atom->object->value, otherV) == 0)
                    {
                        m->variables[beta->variables->joinVariables->size + k] = leftMatch->object;
                    }

                    if (beta->rightPredecessor->atom->subject->type == VARIABLE && strcmp(beta->rightPredecessor->atom->subject->value, otherV) == 0)
                    {
                        m->variables[beta->variables->joinVariables->size + k] = rightMatch->subject;
                    }

                    if (beta->rightPredecessor->atom->predicate->type == VARIABLE && strcmp(beta->rightPredecessor->atom->predicate->value, otherV) == 0)
                    {
                        m->variables[beta->variables->joinVariables->size + k] = rightMatch->predicate;
                    }

                    if (beta->rightPredecessor->atom->object->type == VARIABLE && strcmp(beta->rightPredecessor->atom->object->value, otherV) == 0)
                    {
                        m->variables[beta->variables->joinVariables->size + k] = rightMatch->object;
                    }

                    currOtherVar = currOtherVar->next;
                    k++;
                }

                m->leftFactCause = leftMatch;
                m->rightCause = rightMatch;

                pushLinkedList(betaMatches, m);
                checkNext = true;
            }
            else
            {
                destroyBetaMatch(m);
            }

            currRightMatch = currRightMatch->next;
        }

        currLeftMatch = currLeftMatch->next;
    }

    return checkNext;
}

bool checkBetaNodeFromOneAlphaNode(BetaNode* beta, FactLinkedList* alphaMatches, size_t limit, LinkedList* betaMatches)
{
    bool checkNext = false;

    AlphaNode* pred = (AlphaNode*)beta->alphaLeftPredecessor;
    FactLinkedListNode* currMatch = alphaMatches->first;
    int count = 0;
    while (currMatch != NULL && count < limit)
    {
        count++;
        Fact* f = (Fact*)currMatch->data;
        BetaMatch* m = createBetaMatch(beta->variables->joinVariables->size);

        LinkedListNode* currVar = beta->variables->joinVariables->first;
        int i = 0;
        while (currVar != NULL)
        {
            char* v = (char*)currVar->data;

            // Check subject
            if (pred->atom->subject->type == VARIABLE && strcmp(v, pred->atom->subject->value) == 0)
            {
                m->variables[i] = f->subject;
            }

            // Check predicate
            if (pred->atom->predicate->type == VARIABLE && strcmp(v, pred->atom->predicate->value) == 0)
            {
                m->variables[i] = f->predicate;
            }

            // Check object
            if (pred->atom->object->type == VARIABLE && strcmp(v, pred->atom->object->value) == 0)
            {
                m->variables[i] = f->object;
            }

            currVar = currVar->next;
            i++;
        }

        m->leftFactCause = f;

        pushLinkedList(betaMatches, m);
        checkNext = true;
        
        currMatch = currMatch->next;
    }

    return checkNext;
}

bool checkBetaNodeFromAlphaAndBeta(BetaNode* beta, LinkedList* leftMatches, FactLinkedList* rightMatches, size_t leftLimit, size_t rightLimit, LinkedList* betaMatches)
{
    bool checkNext = false;

    FactLinkedListNode* currRightMatch = rightMatches->first;
    int countRight = 0;
    while (currRightMatch != NULL && countRight < rightLimit)
    {
        countRight++;
        Fact* rightMatch = (Fact*)currRightMatch->data;
        LinkedListNode* currLeftMatch = leftMatches->first;
        int countLeft = 0;

        while (currLeftMatch != NULL && countLeft < leftLimit)
        {
            countLeft++;
            BetaMatch* leftMatch = (BetaMatch*)currLeftMatch->data;
            bool match = false;
            int nbEffectiveJoinVariables = 0;

            BetaMatch* m = createBetaMatch(beta->variables->joinVariables->size + beta->variables->otherVariables->size);

            LinkedListNode* currJoinVar = beta->variables->joinVariables->first;
            int k = 0;
            while (currJoinVar != NULL)
            {
                char* joinV = (char*)currJoinVar->data;
                //Check suject
                if (beta->rightPredecessor->atom->subject->type == VARIABLE)
                {
                    LinkedListNode* currLeftJoin = beta->betaLeftPredecessor->variables->joinVariables->first;
                    int l = 0;
                    while (currLeftJoin != NULL)
                    {
                        char* leftJ = (char*)currLeftJoin->data;
                        if (strcmp(joinV, leftJ) == 0)
                        {
                            if (strcmp(joinV, beta->rightPredecessor->atom->subject->value) == 0)
                            {
                                if (termEquals(leftMatch->variables[l], rightMatch->subject))
                                {
                                    m->variables[k] = rightMatch->subject;
                                    nbEffectiveJoinVariables++;
                                }
                            }
                        }

                        currLeftJoin = currLeftJoin->next;
                        l++;
                    }

                    LinkedListNode* currLeftOther = beta->betaLeftPredecessor->variables->otherVariables->first;
                    l = 0;
                    while (currLeftOther != NULL)
                    {
                        char* leftO = (char*)currLeftOther->data;
                        if (strcmp(joinV, leftO) == 0)
                        {
                            if (strcmp(joinV, beta->rightPredecessor->atom->subject->value) == 0)
                            {
                                if (termEquals(leftMatch->variables[beta->betaLeftPredecessor->variables->joinVariables->size + l], rightMatch->subject))
                                {
                                    m->variables[k] = rightMatch->subject;
                                    nbEffectiveJoinVariables++;
                                }
                            }
                        }

                        currLeftOther = currLeftOther->next;
                        l++;
                    }
                }

                //Check predicate
                if (beta->rightPredecessor->atom->predicate->type == VARIABLE)
                {
                    LinkedListNode* currLeftJoin = beta->betaLeftPredecessor->variables->joinVariables->first;
                    int l = 0;
                    while (currLeftJoin != NULL)
                    {
                        char* leftJ = (char*)currLeftJoin->data;
                        if (strcmp(joinV, leftJ) == 0)
                        {
                            if (strcmp(joinV, beta->rightPredecessor->atom->predicate->value) == 0)
                            {
                                if (termEquals(leftMatch->variables[l], rightMatch->predicate))
                                {
                                    m->variables[k] = rightMatch->predicate;
                                    nbEffectiveJoinVariables++;
                                }
                            }
                        }

                        currLeftJoin = currLeftJoin->next;
                        l++;
                    }

                    LinkedListNode* currLeftOther = beta->betaLeftPredecessor->variables->otherVariables->first;
                    l = 0;
                    while (currLeftOther != NULL)
                    {
                        char* leftO = (char*)currLeftOther->data;
                        if (strcmp(joinV, leftO) == 0)
                        {
                            if (strcmp(joinV, beta->rightPredecessor->atom->predicate->value) == 0)
                            {
                                if (termEquals(leftMatch->variables[beta->betaLeftPredecessor->variables->joinVariables->size + l], rightMatch->predicate))
                                {
                                    m->variables[k] = rightMatch->predicate;
                                    nbEffectiveJoinVariables++;
                                }
                            }
                        }

                        currLeftOther = currLeftOther->next;
                        l++;
                    }
                }

                //Check object
                if (beta->rightPredecessor->atom->object->type == VARIABLE)
                {
                    LinkedListNode* currLeftJoin = beta->betaLeftPredecessor->variables->joinVariables->first;
                    int l = 0;
                    while (currLeftJoin != NULL)
                    {
                        char* leftJ = (char*)currLeftJoin->data;
                        if (strcmp(joinV, leftJ) == 0)
                        {
                            if (strcmp(joinV, beta->rightPredecessor->atom->object->value) == 0)
                            {
                                if (termEquals(leftMatch->variables[l], rightMatch->object))
                                {
                                    m->variables[k] = rightMatch->object;
                                    nbEffectiveJoinVariables++;
                                }
                            }
                        }

                        currLeftJoin = currLeftJoin->next;
                        l++;
                    }

                    LinkedListNode* currLeftOther = beta->betaLeftPredecessor->variables->otherVariables->first;
                    l = 0;
                    while (currLeftOther != NULL)
                    {
                        char* leftO = (char*)currLeftOther->data;
                        if (strcmp(joinV, leftO) == 0)
                        {
                            if (strcmp(joinV, beta->rightPredecessor->atom->object->value) == 0)
                            {
                                if (termEquals(leftMatch->variables[beta->betaLeftPredecessor->variables->joinVariables->size + l], rightMatch->object))
                                {
                                    m->variables[k] = rightMatch->object;
                                    nbEffectiveJoinVariables++;
                                }
                            }
                        }

                        currLeftOther = currLeftOther->next;
                        l++;
                    }
                }

                currJoinVar = currJoinVar->next;
                k++;
            }

            if (nbEffectiveJoinVariables == beta->variables->joinVariables->size)
            {
                match = true;
            }

            if (match)
            {
                if (verboseOn)
                {
                    printf("Found beta match between fact (%s %s %s) and variables { ", rightMatch->subject->value, rightMatch->predicate->value, rightMatch->object->value);
                    for (int i = 0; i < leftMatch->nb_variables; i++)
                    {
                        printf("%s ", leftMatch->variables[i]->value);
                    }
                    printf("}.\n");
                }

                LinkedListNode* currOtherVar = beta->variables->otherVariables->first;
                int k = 0;
                while (currOtherVar != NULL)
                {
                    char* otherV = (char*)currOtherVar->data;

                    LinkedListNode* currLeftJoin = beta->betaLeftPredecessor->variables->joinVariables->first;
                    int l = 0;
                    //Check left beta node variables
                    while (currLeftJoin != NULL)
                    {
                        char* leftJ = (char*)currLeftJoin->data;
                        if (strcmp(leftJ, otherV) == 0)
                        {
                            m->variables[beta->variables->joinVariables->size + k] = leftMatch->variables[l];
                        }

                        currLeftJoin = currLeftJoin->next;
                        l++;
                    }

                    l = 0;
                    LinkedListNode* currLeftOther = beta->betaLeftPredecessor->variables->otherVariables->first;
                    while (currLeftOther != NULL)
                    {
                        char* leftO = (char*)currLeftOther->data;
                        if (strcmp(leftO, otherV) == 0)
                        {
                            m->variables[beta->variables->joinVariables->size + k] = leftMatch->variables[beta->betaLeftPredecessor->variables->joinVariables->size + l];
                        }

                        currLeftOther = currLeftOther->next;
                        l++;
                    }

                    //Check right alpha node variables
                    //Check subject
                    if (beta->rightPredecessor->atom->subject->type == VARIABLE && strcmp(beta->rightPredecessor->atom->subject->value, otherV) == 0)
                    {
                        m->variables[beta->variables->joinVariables->size + k] = rightMatch->subject;
                    }

                    //Check predicate
                    if (beta->rightPredecessor->atom->predicate->type == VARIABLE && strcmp(beta->rightPredecessor->atom->predicate->value, otherV) == 0)
                    {
                        m->variables[beta->variables->joinVariables->size + k] = rightMatch->predicate;
                    }

                    //Check object
                    if (beta->rightPredecessor->atom->object->type == VARIABLE && strcmp(beta->rightPredecessor->atom->object->value, otherV) == 0)
                    {
                        m->variables[beta->variables->joinVariables->size + k] = rightMatch->object;
                    }

                    currOtherVar = currOtherVar->next;
                    k++;
                }

                m->leftMatchCause = leftMatch;
                m->rightCause = rightMatch;

                pushLinkedList(betaMatches, m);
                checkNext = true;
            }
            else
            {
                destroyBetaMatch(m);
            }

            currLeftMatch = currLeftMatch->next;
        }

        currRightMatch = currRightMatch->next;
    }

    return checkNext;
}

void checkTerminalBetaNode(void* rete, BetaNode* beta, LinkedList* matches, FactLinkedList* newFacts, SordModel* kb, Term* insertTermFunction(void*, void*, bool))
{
    LinkedListNode* currMatch = matches->first;
    int count = 0;
    SordWorld* world = sord_get_world(kb);
    while (currMatch != NULL)
    {
        BetaMatch* m = (BetaMatch*)currMatch->data;
        Fact* newFact = createFact(beta->rule->head->subject, beta->rule->head->predicate, beta->rule->head->object);

        int k = 0;
        LinkedListNode* currJoinVar = beta->variables->joinVariables->first;
        while (currJoinVar != NULL)
        {
            char* joinV = (char*)currJoinVar->data;
            if (beta->rule->head->subject->type == VARIABLE)
            {
                if (strcmp(beta->rule->head->subject->value, joinV) == 0)
                {
                    newFact->subject = m->variables[k];
                }
            }

            if (beta->rule->head->predicate->type == VARIABLE)
            {
                if (strcmp(beta->rule->head->predicate->value, joinV) == 0)
                {
                    newFact->predicate = m->variables[k];
                }
            }

            if (beta->rule->head->object->type == VARIABLE)
            {
                if (strcmp(beta->rule->head->object->value, joinV) == 0)
                {
                    newFact->object = m->variables[k];
                }
            }

            currJoinVar = currJoinVar->next;
            k++;
        }

        LinkedListNode* currOtherVar = beta->variables->otherVariables->first;
        k = 0;
        while (currOtherVar != NULL)
        {
            char* otherV = (char*)currOtherVar->data;
            if (beta->rule->head->subject->type == VARIABLE)
            {
                if (strcmp(beta->rule->head->subject->value, otherV) == 0)
                {
                    newFact->subject = m->variables[beta->variables->joinVariables->size + k];
                }
            }

            if (beta->rule->head->predicate->type == VARIABLE)
            {
                if (strcmp(beta->rule->head->predicate->value, otherV) == 0)
                {
                    newFact->predicate = m->variables[beta->variables->joinVariables->size + k];
                }
            }

            if (beta->rule->head->object->type == VARIABLE)
            {
                if (strcmp(beta->rule->head->object->value, otherV) == 0)
                {
                    newFact->object = m->variables[beta->variables->joinVariables->size + k];
                }
            }

            currOtherVar = currOtherVar->next;
            k++;
        }

        // A literal can only be the object of a fact
        if (newFact->subject->type != LITERAL && newFact->predicate->type != LITERAL)
        {
            bool implFound = false;

            if (beta->implicitFacts == NULL)
            {
                beta->implicitFacts = createLinkedList();
                ImplicitFact* impl = createImplicitFact(newFact);
                pushLinkedList(impl->origins, m);
                pushLinkedList(beta->implicitFacts, impl);
            }
            else
            {
                LinkedListNode* currImpl = beta->implicitFacts->first;
                while (currImpl != NULL)
                {
                    ImplicitFact* impl = (ImplicitFact*)currImpl->data;
                    if (factEquals(impl->value, newFact))
                    {
                        implFound = true;
                        destroyFact(newFact, false, false);
                        newFact = impl->value;
                        bool originFound = false;
                        LinkedListNode* currOrigin = impl->origins->first;
                        while (currOrigin != NULL)
                        {
                            BetaMatch* origin = (BetaMatch*)currOrigin->data;
                            if (betaMatchEquals(origin, m))
                            {
                                originFound = true;
                                break;
                            }

                            currOrigin = currOrigin->next;
                        }
                        if (!originFound)
                        {
                            pushLinkedList(impl->origins, m);
                        }
                        break;
                    }
                    currImpl = currImpl->next;
                }
                if (!implFound)
                {
                    ImplicitFact* impl = createImplicitFact(newFact);
                    pushLinkedList(impl->origins, m);
                    pushLinkedList(beta->implicitFacts, impl);
                }
            }
            
            if (!implFound)
            {
                SordWorld* world = sord_get_world(kb);
                SordNode* newSub = getSordNodeFromTerm(newFact->subject, world);
                SordNode* newPred = getSordNodeFromTerm(newFact->predicate, world);
                SordNode* newObj = getSordNodeFromTerm(newFact->object, world);

                if (!sord_ask(kb, newSub, newPred, newObj, 0))
                {
                    SordQuad newQuad = {newSub, newPred, newObj, 0};
                    sord_add(kb, newQuad);
                    count++;

                    if (beta->matchingAlphaNodes != NULL)
                    {
                        LinkedListNode* currAlpha = beta->matchingAlphaNodes->first;
                        while (currAlpha != NULL)
                        {
                            AlphaNode* alpha = (AlphaNode*)currAlpha->data;
                            if (!alpha->shared && matchAtomWithFact(alpha->atom, newFact))
                            {
                                addFact(alpha->newMatches, copyFact(newFact, false, false));
                            }
                            
                            currAlpha = currAlpha->next;
                        }
                    }
                }
                else
                {
                    sord_node_free(world, newSub);
                    sord_node_free(world, newPred);
                    sord_node_free(world, newObj);
                }
            }
        }
        else
        {
            destroyFact(newFact, false, false);
        }

        currMatch = currMatch->next;
    }
}

void checkBetaNodeAddFact(void* rete, BetaNode* beta, void* leftMatches, void* rightMatches, size_t leftLimit, size_t rightLimit, FactLinkedList* newFacts, SordModel* kb, Term* insertTermFunction(void*, void*, bool))
{
    // both beta's parents are alpha nodes
    if (beta->fromAlpha)
    {
        // beta has two (alpha) parents
        if (beta->rightPredecessor)
        {
            bool checkNext = checkBetaNodeFromTwoAlphaNodes(beta, (FactLinkedList*)leftMatches, (FactLinkedList*)rightMatches, leftLimit, rightLimit, beta->newMatches);
            if (checkNext && beta->successor != NULL)
            {
                checkBetaNodeAddFact(rete, beta->successor, beta->newMatches, beta->successor->rightPredecessor->matches, beta->newMatches->size, beta->successor->rightPredecessor->matches->size, newFacts, kb, insertTermFunction);

                if (beta->successor->rightPredecessor->triggered && beta->successor->rightPredecessor->checked)
                {
                    checkBetaNodeAddFact(rete, beta->successor, beta->newMatches, beta->successor->rightPredecessor->newMatches, beta->newMatches->size, beta->successor->rightPredecessor->oldNewMatchesSize, newFacts, kb, insertTermFunction);
                }
            }
        }
        // beta has only one (alpha) parent
        else
        {
            checkBetaNodeFromOneAlphaNode(beta, (FactLinkedList*)leftMatches, leftLimit, beta->newMatches);
        }
    }
    // beta's left parent is a beta node and its right parent is an alpha node
    else
    {
        bool checkNext = checkBetaNodeFromAlphaAndBeta(beta, (LinkedList*)leftMatches, (FactLinkedList*)rightMatches, leftLimit, rightLimit, beta->newMatches);
        if (checkNext && beta->successor != NULL)
        {
            checkBetaNodeAddFact(rete, beta->successor, beta->newMatches, beta->successor->rightPredecessor->matches, beta->newMatches->size, beta->rightPredecessor->matches->size, newFacts, kb, insertTermFunction);

            if (beta->successor->rightPredecessor->triggered && beta->successor->rightPredecessor->checked)
            {
                checkBetaNodeAddFact(rete, beta->successor, beta->newMatches, beta->successor->rightPredecessor->newMatches, beta->newMatches->size, beta->successor->rightPredecessor->oldNewMatchesSize, newFacts, kb, insertTermFunction);
            }
        }
    }
    // beta is a terminal node
    if (beta->terminalNode)
    {
        checkTerminalBetaNode(rete, beta, beta->newMatches, newFacts, kb, insertTermFunction);
    }

    moveBetaMatches(beta);
}

void checkBetaNodeRemoveFact(BetaNode* beta, bool fromLeft, SordModel* kb, LinkedList* terminalBetaNodes)
{
    bool checkNext = false;

    // beta only has alpha predecessor(s)
    if (beta->fromAlpha)
    {
        // beta has a unique alpha predecessor
        if (beta->rightPredecessor == NULL)
        {
            checkNext = checkBetaNodeFromOneAlphaNodeRemoveFact(beta);
        }
        // beta has two alpha predecessors
        else
        {
            FactLinkedList* alphaMatches = fromLeft ? beta->alphaLeftPredecessor->deletedMatches : beta->rightPredecessor->deletedMatches;
            size_t limit = fromLeft ? beta->alphaLeftPredecessor->oldDeletedMatchesSize : beta->rightPredecessor->oldDeletedMatchesSize;
            checkNext = checkBetaNodeFromTwoAlphaNodesRemoveFact(beta, alphaMatches, limit, fromLeft);
        }
    }
    // beta has a beta and an alpha predecessor
    else
    {
        checkNext = checkBetaNodeFromAlphaAndBetaRemoveFact(beta, fromLeft);
    }

    if (beta->terminalNode)
    {
        checkTerminalBetaNodeRemoveFact(beta, kb, terminalBetaNodes);
    }

    if (checkNext && beta->successor != NULL)
    {
        checkBetaNodeRemoveFact(beta->successor, true, kb, terminalBetaNodes);
    }

    moveBetaMatches(beta);
}

bool checkBetaNodeFromTwoAlphaNodesRemoveFact(BetaNode* beta, FactLinkedList* alphaMatches, size_t limit, bool fromLeft)
{
    bool checkNext = false;

    FactLinkedListNode* currParentMatch = alphaMatches->first;

    int count = 0;
    while (currParentMatch != NULL && count < limit)
    {
        count++;
        Fact* deletedFact = (Fact*)currParentMatch->data;
        LinkedListNode* currBetaMatch = beta->matches->first;
        while (currBetaMatch != NULL)
        {
            BetaMatch* m = (BetaMatch*)currBetaMatch->data;
            // printf("TWO ALPHAS ");
            // printFact(deletedFact);
            // printf("%d RIGHT CAUSE ", fromLeft);
            // printFact(m->rightCause);
            // printf("\n");
            if ((fromLeft && factEquals(m->leftFactCause, deletedFact)) || (!fromLeft && factEquals(m->rightCause, deletedFact)))
            {
                currBetaMatch = deleteLinkedListCursor(beta->matches, currBetaMatch);
                pushLinkedList(beta->deletedMatches, m);
                // printf("ok two alpha\n");
                checkNext = true;
            }
            else
            {
                currBetaMatch = currBetaMatch->next;
            }            
        }

        currParentMatch = currParentMatch->next;
    }
    
    return checkNext;
}

bool checkBetaNodeFromOneAlphaNodeRemoveFact(BetaNode* beta)
{
    bool checkNext = false;

    FactLinkedListNode* currAlphaMatch = beta->alphaLeftPredecessor->deletedMatches->first;
    int count = 0;
    while (currAlphaMatch != NULL && count < beta->alphaLeftPredecessor->oldDeletedMatchesSize)
    {
        count++;
        Fact* deletedFact = (Fact*)currAlphaMatch->data;

        LinkedListNode* currBetaMatch = beta->matches->first;
        while (currBetaMatch != NULL)
        {
            BetaMatch* m = (BetaMatch*)currBetaMatch->data;
            if (factEquals(m->leftFactCause, deletedFact))
            {
                currBetaMatch = deleteLinkedListCursor(beta->matches, currBetaMatch);
                pushLinkedList(beta->deletedMatches, m);
                // printf("ok one alpha\n");
                checkNext = true;
            }
            else
            {
                currBetaMatch = currBetaMatch->next;
            }
        }
        currAlphaMatch = currAlphaMatch->next;
    }

    return checkNext;
}

bool checkBetaNodeFromAlphaAndBetaRemoveFact(BetaNode* beta, bool fromLeft)
{
    bool checkNext = false;

    if (fromLeft)
    {
        LinkedListNode* currParentMatch = beta->betaLeftPredecessor->deletedMatches->first;
        while (currParentMatch != NULL)
        {
            BetaMatch* parentMatch = (BetaMatch*)currParentMatch->data;

            LinkedListNode* currMatch = beta->matches->first;
            while (currMatch != NULL)
            {
                BetaMatch* m = (BetaMatch*)currMatch->data;
                if (m->leftMatchCause == parentMatch)
                {
                    currMatch = deleteLinkedListCursor(beta->matches, currMatch);
                    pushLinkedList(beta->deletedMatches, m);
                    // printf("ok alpha beta\n");
                    checkNext = true;
                }
                else
                {
                    currMatch = currMatch->next;
                }
            }

            currParentMatch = currParentMatch->next;
        }
    }
    else
    {
        FactLinkedListNode* currParentMatch = beta->rightPredecessor->deletedMatches->first;
        int count = 0;
        while (currParentMatch != NULL && count < beta->rightPredecessor->oldDeletedMatchesSize)
        {
            count++;
            Fact* deletedFact = (Fact*)currParentMatch->data;

            LinkedListNode* currMatch = beta->matches->first;
            while (currMatch != NULL)
            {
                BetaMatch* m = (BetaMatch*)currMatch->data;
                if (factEquals(m->rightCause, deletedFact))
                {
                    currMatch = deleteLinkedListCursor(beta->matches, currMatch);
                    pushLinkedList(beta->deletedMatches, m);
                    checkNext = true;
                }
                else
                {
                    currMatch = currMatch->next;
                }
            }

            currParentMatch = currParentMatch->next;
        }
    }

    return checkNext;
}

void checkTerminalBetaNodeRemoveFact(BetaNode* beta, SordModel* kb, LinkedList* terminalBetaNodes)
{
    if (beta->implicitFacts != NULL)
    {
        LinkedListNode* currDeletedMatch = beta->deletedMatches->first;
        while (currDeletedMatch != NULL)
        {
            BetaMatch* deletedMatch = (BetaMatch*)currDeletedMatch->data;

            LinkedListNode* currImpl = beta->implicitFacts->first;
            while (currImpl != NULL)
            {
                ImplicitFact* impl = (ImplicitFact*)currImpl->data;
                // printf("TERMINAL ");
                // printFact(impl->value);
                // printFact(deletedMatch->rightCause);
                // printf("\n");
                
                LinkedListNode* currOrigin = impl->origins->first;
                while (currOrigin != NULL)
                {
                    BetaMatch* origin = (BetaMatch*)currOrigin->data;
                    // if (betaMatchEquals(origin, deletedMatch))
                    if (origin == deletedMatch)
                    {
                        // printf("found origin\n\n");
                        currOrigin = deleteLinkedListCursor(impl->origins, currOrigin);
                        break;
                    }
                    else
                    {
                        currOrigin = currOrigin->next;
                    }
                }

                if (impl->origins->size == 0)
                {
                    // printf("no more origins\n\n");
                    bool implFoundOtherBeta = false;

                    LinkedListNode* currOtherBeta = terminalBetaNodes->first;
                    while (currOtherBeta != NULL)
                    {
                        BetaNode* otherBeta = (BetaNode*)currOtherBeta->data;
                        if (otherBeta != beta && matchAtomWithFact(otherBeta->rule->head, impl->value))
                        {
                            if (otherBeta->implicitFacts != NULL)
                            {
                                LinkedListNode* currOtherImpl = otherBeta->implicitFacts->first;
                                while (currOtherImpl != NULL)
                                {
                                    ImplicitFact* otherImpl = (ImplicitFact*)currOtherImpl->data;
                                    if (factEquals(impl->value, otherImpl->value))
                                    {
                                        // implFoundOtherBeta = false;
                                        // printf("found other impl\n");
                                        // printFact(beta->rule->head);
                                        // printFact(otherBeta->rule->head);
                                        // printf("\n");

                                        for (LinkedListNode* currOtherOrigin = otherImpl->origins->first; currOtherOrigin != NULL; currOtherOrigin = currOtherOrigin->next)
                                        {
                                            BetaMatch* otherOrigin = (BetaMatch*)currOtherOrigin->data;
                                            // printf("OTHER ORIGIN ");
                                            // printFact(otherOrigin->leftFactCause);
                                            // printFact(otherOrigin->rightCause);

                                            if (!betaMatchContainsOrigin(otherOrigin, impl->value))
                                            {
                                                implFoundOtherBeta = true;
                                                break;
                                            }
                                        }
                                        break;
                                    }

                                    currOtherImpl = currOtherImpl->next;
                                }
                            }
                        }

                        if (implFoundOtherBeta)
                        {
                            break;
                        }

                        currOtherBeta = currOtherBeta->next;
                    }
                    //printFact(impl->value);

                    if (!implFoundOtherBeta)
                    {
                        SordWorld* world = sord_get_world(kb);
                        SordQuad quad = {
                            getSordNodeFromTerm(impl->value->subject, world),
                            getSordNodeFromTerm(impl->value->predicate, world),
                            getSordNodeFromTerm(impl->value->object, world),
                            NULL
                        };

                        if (sord_ask(kb, quad[0], quad[1], quad[2], NULL))
                        {
                            sord_remove(kb, quad);
                        }

                        if (beta->matchingAlphaNodes != NULL)
                        {
                            for (LinkedListNode* currAlpha = beta->matchingAlphaNodes->first; currAlpha != NULL; currAlpha = currAlpha->next)
                            {
                                AlphaNode* alpha = (AlphaNode*)currAlpha->data;
                                if (!alpha->shared && matchAtomWithFact(alpha->atom, impl->value))
                                {
                                    addFact(alpha->deletedMatches, copyFact(impl->value, false, false));
                                }
                            }
                        }
                    }
                    destroyFact(impl->value, false, false);
                    destroyImplicitFact(impl);
                    currImpl = deleteLinkedListCursor(beta->implicitFacts, currImpl);
                }
                else
                {
                    currImpl = currImpl->next;
                }
            }
            currDeletedMatch = currDeletedMatch->next;
        }
    }
}



void moveBetaMatches(BetaNode* beta)
{
    // Move new matches
    LinkedListNode* currMatch = beta->newMatches->first;
    while (currMatch != NULL)
    {
        BetaMatch* m = (BetaMatch*)currMatch->data;
        pushLinkedList(beta->matches, m);
        currMatch = deleteLinkedListCursor(beta->newMatches, currMatch);
    }

    // Remove deleted matches
    currMatch = beta->deletedMatches->first;
    while (currMatch != NULL)
    {
        destroyBetaMatch((BetaMatch*)currMatch->data);
        currMatch = deleteLinkedListCursor(beta->deletedMatches, currMatch);
    }
}
