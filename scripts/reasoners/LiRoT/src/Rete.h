#include <string.h>
#include <stdlib.h>
#include "LinkedList.h"
#include "Fact.h"
#include "FactLinkedList.h"
#include "Verbose.h"
#include "Utils.h"
#include "Rule.h"
#include "KnowledgeBase.h"
#include "AlphaNode.h"
#include "BetaNode.h"
#include "uthash.h"
#include "zix/digest.h"
#include "sord/sord.h"
#include "sord_internal.h"

#ifndef RETE_H_INCLUDED
#define RETE_H_INCLUDED

// ---------------------------------------------- Structures ---------------------------------------------------

typedef struct HashElement
{
    int key;
    Term* term;
    UT_hash_handle hh;
} HashElement;

/**
 * Implementation of a Rete network.
 * Alpha and beta nodes are placed in distinct data structures.
 */
typedef struct Rete
{
    /**
     * Alpha nodes in the Rete trie (each node is associated to an atomic condition in a rule).
     */
    LinkedList* alphaNodes;

    /**
     * Beta nodes in the Rete trie (each beta node joins 2 alpha nodes or a beta node and an alpha node).
     */
    LinkedList* betaNodes;

    LinkedList* terminalBetaNodes;

    /**
     * Ruleset used for inference.
     */
    LinkedList* rules;

    /**
     * Knowledge base.
     */
    SordModel* kb;

    HashElement* terms;
} Rete;

// ------------------------------------------------ Functions --------------------------------------------------

/**
 * \relates Rete
 * Returns a linked list containing all alpha nodes associated to rule r.
 */
LinkedList* findAlphaNodesByRule(Rete* rete, Rule* r);

// ----------- Rete creation functions ------------

/**
 * \relates Rete
 * Creates a Rete network from an array of rules.
 * @param rules The ruleset used by the Rete network.
 * @param kb The knowledge base used by the Rete network.
 */
Rete* createReteNetwork(LinkedList* rules, SordModel* kb);

static uint32_t
sord_node_hash(const void* n);

Term* insertTerm(void* rete, void* node, bool destroyIfExists);

// ------------ Inferrence functions --------------

/**
 * \relates Rete
 * Runs the Rete algorithm until no new facts are inferred.
 * @return The list of inferred facts.
 */

void addExplicitFacts(Rete* rete, SordModel* newFacts);

void removeExplicitFacts(Rete* rete, SordModel* deletedFacts);

/**
 * \relates Rete
 * Removes all traces from previous reasoning tasks.
 */
void cleanRete(Rete* rete);

// ----------------- Destructors ------------------

/**
 * \relates Rete
 * Frees dynamic memory allocated to a Rete network.
 */
void destroyRete(Rete* rete);

#endif // !RETE_H_INCLUDED