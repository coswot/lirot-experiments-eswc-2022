#include "Utils.h"
#include <stdio.h>

bool startsWith(const char* str, const char* prefix)
{
    return strncmp(prefix, str, strlen(prefix)) == 0;
}

LinkedList* splitStr(char* str, char* delimiter)
{
	LinkedList* res = createLinkedList();

	char* token = strtok(str, delimiter);
	while (token != NULL)
	{
        if (token[strlen(token) - 1] == '\r')
        {
            token[strlen(token) - 1] = 0;
        }
        
		char* copy = (char*)malloc((strlen(token) + 1) * sizeof(char));
		strcpy(copy, token);
		pushLinkedList(res, copy);
		token = strtok(NULL, delimiter);
	}

	return res;
}

char* initString(char* str)
{
	char* res = (char*)malloc((strlen(str) + 1) * sizeof(char));
	if (res)
	{
		strcpy(res, str);
	}

	return res;
}

char* copyString(char* str, char* dest)
{
	if (strlen(dest) != strlen(str))
	{
		char* copy = (char*)realloc(dest, (strlen(str) + 1) * sizeof(char));
		if (copy)
		{
			dest = copy;
			strcpy(dest, str);
		}
	}
	else
	{
		strcpy(dest, str);
	}

	return dest;
}

LinkedList* getLines(char* path)
{
	LinkedList* lines = createLinkedList();

	FILE* f;
	char line[1000];

	f = fopen(path, "r");
	if (f)
	{
		while(fgets(line, sizeof(line), f))
		{
		    line[strlen(line) - 1] = '\0'; // remove \n at the end;
			char* lineCopy = (char*)malloc((strlen(line) + 1) * sizeof(char));
			if (lineCopy)
			{
				strcpy(lineCopy, line);
				pushLinkedList(lines, lineCopy);
			}
		}

		fclose(f);
	}
	else
	{
		printf("Error reading %s.\n", path);
	}

	return lines;
}

double getDoubleFromTerm(char* term)
{
	/*
    char* termCopy = initString(term);
    char* splitTerm = strtok(termCopy, "^^");
    splitTerm = strtok(splitTerm, "\"");
    splitTerm = strtok(splitTerm, "\"");
    double result = atof(splitTerm);
    free(termCopy);
	*/

	double result = atof(term);

    return result;
}