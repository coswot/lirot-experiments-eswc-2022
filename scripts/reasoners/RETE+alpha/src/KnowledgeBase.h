#include <stdlib.h>
#include "KnowledgeBaseNode.h"
#include "FactLinkedList.h"

#ifndef KNOWLEDGEBASE_H_INCLUDED
#define KNOWLEDGEBASE_H_INCLUDED

/*
* KnowledgeBase: fact linked list implementation. A list has a first node, a last node (that makes push insertion faster) and a size.
* Elements in a list can have different types.
* Example of iteration through a linked list:
*
* KnowledgeBase* l; // list of int in this case)
* KnowledgeBaseNode* current = l.first;
* while (current != NULL)
* {
*	printFact(current.data);
*	current = current-> next;
* }
*/
typedef struct KnowledgeBase
{
	struct KnowledgeBaseNode* first;
	struct KnowledgeBaseNode* last;
	size_t size;
} KnowledgeBase;

KnowledgeBase* getFactsKB(LinkedList* lines);

char* getVariableValueKB(char* variable, KnowledgeBase* facts);

// ---------- KnowledgeBase functions ------------

/*
* Returns a pointer to an empty LinkedList.
*/
KnowledgeBase* createKnowledgeBase();

/*
* Inserts a new element at the specified index. If index is the size of the list, the element will be added at the end.
*/
void insertKnowledgeBaseAtIndex(KnowledgeBase* kb, Fact* element, int index);

/*
* Returns the index-th element of the list.
*/
void* getKnowledgeBaseAtIndex(KnowledgeBase* kb, int index);

/*
* Removes the element at the specified index in the list.
* If freeContent is true, then the memory space allocated to the corresponding element will be freed.
*/
void removeKnowledgeBaseAtIndex(KnowledgeBase* kb, int index);

// ----------------- Destructors ------------------

/*
* Frees the memory space allocated to the list.
*/
void freeKnowledgeBase(KnowledgeBase* kb);

// --------------------- API ----------------------

bool addFactToKB(KnowledgeBase* kb, Fact* element);

bool deleteFactFromKB(KnowledgeBase* kb, Fact* element);

bool KBHasFact(KnowledgeBase* kb, Fact* element);

FactLinkedList* matchFactKB(KnowledgeBase* kb, Fact* pattern);

#endif // !KNOWLEDGEBASE_H_INCLUDED