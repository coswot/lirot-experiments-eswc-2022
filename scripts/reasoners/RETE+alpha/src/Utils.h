#include "LinkedList.h"

#ifndef UTILS_H_INCLUDED
#define UTILS_H_INCLUDED

/**
 * Checks if a string starts with a specific substring.
 * @return True if str starts with prefix, false otherwise.
 */
bool startsWith(const char* str, const char* prefix);

/**
 * Check if an array contains a string.
 * @return true if str is in arr, false otherwise.
 */
bool isInArray(char** arr, int arrLength, char* str);

/**
 * Splits a string and puts its elements in a linked list.
 */
LinkedList* splitStr(char* str, char* delimiter);

char* copyString(char* str, char* dest);

/**
 * Clones a string.
 * @return The copy of the string.
 */
char* initString(char* str);

/**
 * Extracts every line in a file.
 * @return A linked list of lines. Each line is a char*.
 */
LinkedList* getLines(char* path);

/**
 * Converts a string to a numerical value.
 */
double getDoubleFromTerm(char* term);

#endif // !UTILS_H_INCLUDED