#include <stdlib.h>
#include "FactLinkedListNode.h"

#ifndef FACTLINKEDLIST_H_INCLUDED
#define FACTLINKEDLIST_H_INCLUDED

/**
 * Fact linked list implementation.
 * Elements in a list can have different types.
 * 
 * Example of iteration through a linked list:
 *
 * \code{.c}
 * FactLinkedList* l;
 * FactLinkedListNode* current = l->first;
 * while (current != NULL)
 * {
 *	printFact(current->data);
 *	current = current->next;
 * }
 * \endcode
 */
typedef struct FactLinkedList
{
	/**
	 * First element in the list.
	 */
	struct FactLinkedListNode* first;

	/**
	 * Last element in the list.
	 * It is useful to insert a new element at the end of the list.
	 */
	struct FactLinkedListNode* last;

	/**
	 * Number of elements in the list.
	 */
	size_t size;
} FactLinkedList;

/**
 * \relates FactLinkedList
 * Extracts facts from text.
 * 
 * Each line must be of the form "subject,predicate,object".
 */
FactLinkedList* getFacts(LinkedList* lines);

// ---------- FactLinkedList functions ------------

/**
 * \relates FactLinkedList
 * Returns a pointer to an empty FactLinkedList.
 */
FactLinkedList* createFactLinkedList();

/**
 * \relates FactLinkedList
 * Inserts a new element at the specified index.
 * If index is the size of the list, the element will be added at the end.
 */
void insertFactLinkedListAtIndex(FactLinkedList* l, Fact* element, int index);

/**
 * \relates FactLinkedList
 * Returns the index-th element of the list.
 */
void* getFactLinkedListAtIndex(FactLinkedList* l, int index);

/**
 * \relates FactLinkedList
 * Removes the element at the specified index in the list.
*/
void removeFactLinkedListAtIndex(FactLinkedList* l, int index);

// ----------------- Destructors ------------------

/**
 * \relates FactLinkedList
 * Frees the memory space allocated to the list.
 */
void freeFactLinkedList(FactLinkedList* l);

// --------------------- API ----------------------

/**
 * \relates FactLinkedList
 * Adds a fact at the end of the list.
 */
void addFact(FactLinkedList* facts, Fact* element);

/**
 * \relates FactLinkedList
 * Removes a fact from a list (if present).
 */
void deleteFact(FactLinkedList* facts, Fact* element);

FactLinkedListNode* deleteFactCursor(FactLinkedList* facts, FactLinkedListNode* cursor);

/**
 * \relates FactLinkedList
 * Finds if an element is present in the list.
 * \return The desired fact if present in the list; Null otherwise.
 */
Fact* hasFact(FactLinkedList* facts, Fact* element);

#endif // !FACTLINKEDLIST_H_INCLUDED