#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#ifndef LINKEDLIST_H_INCLUDED
#define LINKEDLIST_H_INCLUDED

// ---------------------------------------------- Structures ---------------------------------------------------

/**
 * Structure repesenting an element in a LinkedList.
 */
typedef struct LinkedListNode
{
	/**
	 * Reference to the actual data embedded in the node.
	 */
	void* data;

	/**
	 * Reference to the next element in the list.
	 */
	struct LinkedListNode* next;
} LinkedListNode;

/**
 * Structure representing a simple generic linked list implementation/
 * A list can contain pointers to any type (including structures).
 * Elements in a list can have different types.
 * 
 * Example of iteration through a linked list:
 *
 * \code{.c}
 * LinkedList* l;
 * LinkedListNode* current = l->first;
 * while (current != NULL)
 * {
 *	// do stuff
 *	current = current-> next;
 * }
 * \endcode
 */
typedef struct LinkedList
{
	/**
	 * First element in the list.
	 */
	struct LinkedListNode* first;

	/**
	 * Last element in the list.
	 * It is useful to insert a new element at the end of the list.
	 */
	struct LinkedListNode* last;

	/**
	 * Number of elements in the list.
	 */
	size_t size;
} LinkedList;

// ---------------------------------------------- Functions ----------------------------------------------------

// --------------- Linked List -----------------

/**
 * \relates LinkedList
 * Returns a pointer to an empty LinkedList.
 */
LinkedList* createLinkedList();

/**
 * \relates LinkedList
 * Inserts an element at the end of the list.
 */
void pushLinkedList(LinkedList* l, void* element);

LinkedListNode* deleteLinkedListCursor(LinkedList* l, LinkedListNode* cursor);

/**
 * \relates LinkedList
 * Inserts a new element at the specified index.
 * If index is the size of the list, the element will be added at the end.
 */
void insertLinkedListAtIndex(LinkedList* l, void* element, int index);

/**
 * \relates LinkedList
 * Finds if an element is present in the list.
 * 
 * \warning Equality is assessed based on the pointer to the element.
 * @return True if the desired element is found in the list; false otherwise.
 */
bool LinkedListContains(LinkedList* l, void* element);

/**
 * \relates LinkedList
 * Finds if a string is present in the list.
 * 
 * \warning String equality is used to find the element in the list, not pointer equality.
 * @return True if the desired element is found in the list; false otherwise.
 */
bool LinkedListContainsString(LinkedList* l, char* element);

/**
 * \relates LinkedList
 * Returns the index-th element of the list.
 */
void* getLinkedListAtIndex(LinkedList* l, int index);

/**
 * \relates LinkedList
 * Removes the element at the specified index in the list.
*/
void removeLinkedListAtIndex(LinkedList* l, int index);

/**
 * \relates LinkedList
 * Frees the memory space allocated to the list.
 */
void freeLinkedList(LinkedList* l);

#endif // !LINKEDLIST_H_INCLUDED