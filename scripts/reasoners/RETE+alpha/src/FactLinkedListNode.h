#include "Fact.h"

#ifndef FACTLINKEDLISTNODE_H_INCLUDED
#define FACTLINKEDLISTNODE_H_INCLUDED

/**
* Structure representing an element in a FactLinkedList.
*/
typedef struct FactLinkedListNode
{
	/**
	 * Reference to the actual data embedded in the node.
	 */
	Fact* data;

	/**
	 * Reference to the next element in the list.
	 */
	struct FactLinkedListNode* next;
} FactLinkedListNode;

#endif // !FACTLINKEDLISTNODE_H_INCLUDED