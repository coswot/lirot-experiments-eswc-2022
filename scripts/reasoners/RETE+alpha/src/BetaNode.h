#include "BetaVariables.h"
#include "BetaMatch.h"
#include "AlphaNode.h"

#ifndef BETANODE_H_INCLUDED
#define BETANODE_H_INCLUDED

/**
 * Structure representing a beta node in a Rete network. Each beta node joins 2 previous nodes (either 2 alpha nodes or an alpha and a beta node).
 */
typedef struct BetaNode
{
    /**
     * Reference to the next beta node in the network.
     */
    struct BetaNode* successor;

    /**
     * If this node joins 2 alpha nodes, then it is a reference to its left predecessor. Null otherwise.
     */
    struct AlphaNode* alphaLeftPredecessor;

    /**
     * If this node joins an alpha and a beta node, then it is a reference to its left beta predecessor. Null otherwise.
     */
    struct BetaNode* betaLeftPredecessor;

    /**
     * A reference to its right alpha predecessor.
     */
    struct AlphaNode* rightPredecessor;

    /**
     * True if its left predecessor is an alpha node, false otherwise.
     */
    bool fromAlpha;

    /**
     * True if it has no successor, false otherwise.
     */
    bool terminalNode;

    /**
     * Reference to the associated rule.
     */
    struct Rule* rule;

    /**
     * Structure containing all the variables associated to its rule.
     */
    struct BetaVariables* variables;

    /**
     * Linked list of combinations of values that match the node's variables.
     * Actual type: LinkedList<BetaMatch*>
     */
    LinkedList* matches; // LinkedList<BetaMatch*>

    LinkedList* newMatches;

    LinkedList* deletedMatches;

    LinkedList* implicitFacts;

    LinkedList* matchingAlphaNodes; // alpha nodes which condition is similar to the rule's conclusion
} BetaNode;

/**
 * \relates BetaNode
 * Creates a beta node that has 2 alpha predecessors.
 * @param rule reference to the rule associated to the node.
 * @param leftNode reference to the node's left predecessor.
 * @param rightNode: reference to the node's right predecessor.
 */
BetaNode* createBetaNodeFromAlpha(Rule* rule, AlphaNode* leftNode, AlphaNode* rightNode);

/**
 * \relates BetaNode
 * Creates a beta node that has a beta predecessor to the left and an alpha predecessor to the right.
 * @param rule reference to the rule associated to the node.
 * @param leftNode reference to the node's left predecessor.
 * @param rightNode: reference to the node's right predecessor.
 */
BetaNode* createBetaNodeFromBeta(Rule* rule, BetaNode* leftNode, AlphaNode* rightNode);

/**
 * \relates BetaNode
 * Checks all the possible matches for a beta node using its predecessors.
 * If beta is a terminal node, matches are inserted in newFacts (if not already present).
 * @param beta the considered beta node.
 * @param newFacts the list of new facts entailed during inference (not necessarily empty).
 */
bool checkBetaNode(BetaNode* beta, FactLinkedList* newFacts, SordModel* kb);

bool checkBetaNodeFromTwoAlphaNodes(BetaNode* beta, FactLinkedList* leftMatches, FactLinkedList* rightMatches, size_t leftLimit, size_t rightLimit, LinkedList* betaMatches);

bool checkBetaNodeFromOneAlphaNode(BetaNode* beta, FactLinkedList* alphaMatches,size_t limit, LinkedList* betaMatches);

bool checkBetaNodeFromAlphaAndBeta(BetaNode* beta, LinkedList* leftMatches, FactLinkedList* rightMatches, size_t leftLimit, size_t rightLimit, LinkedList* betaMatches);

void checkTerminalBetaNode(BetaNode* beta, LinkedList* matches, FactLinkedList* newFacts, SordModel* kb);

void checkBetaNodeAddFact(BetaNode* beta, void* leftMatches, void* rightMatches, size_t leftLimit, size_t rightLimit, FactLinkedList* newFacts, SordModel* kb);

// ------------------ Deletion functions --------------------

void checkBetaNodeRemoveFact(BetaNode* beta, bool fromLeft, SordModel* kb, LinkedList* terminalBetaNodes);

bool checkBetaNodeFromTwoAlphaNodesRemoveFact(BetaNode* beta, FactLinkedList* alphaMatches, size_t limit, bool fromLeft);

bool checkBetaNodeFromOneAlphaNodeRemoveFact(BetaNode* beta);

bool checkBetaNodeFromAlphaAndBetaRemoveFact(BetaNode* beta, bool fromLeft);

void checkTerminalBetaNodeRemoveFact(BetaNode* beta, SordModel* kb, LinkedList* terminalBetaNodes);

bool checkImplicitFactOtherBetaNodes(BetaNode* beta, ImplicitFact* impl, LinkedList* terminalBetaNodes);


void moveBetaMatches(BetaNode* beta);

/**
 * \related BetaNode
 * Identifies the common variables between 2 alpha nodes' conditions, and places them in result.
 * @param left the first alpha node to consider.
 * @param right the second alpha node to consider.
 * @returns The variables that the 2 alpha nodes have in common.
 */
BetaVariables* getCommonVariablesAlpha(AlphaNode* left, AlphaNode* right);

/**
 * \relates BetaNode
 * Identifies the common variables between a beta node and an alpha node, and places them in result.
 * All variables from the left beta node(join and other variables) are checked.
 * @param left the beta node to consider.
 * @param right the alpha node to consider.
 * @returns The variables that the 2 nodes have in common.
 */
BetaVariables* getCommonVariablesBeta(BetaNode* left, AlphaNode* right);

#endif // !BETANODE_H_INCLUDED
