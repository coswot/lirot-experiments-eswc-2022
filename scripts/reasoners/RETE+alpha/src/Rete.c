#include "Rete.h"

Rete* createReteNetwork(LinkedList* rules, SordModel* kb)
{
    if (verboseOn)
    {
        printf("Enter function createReteNetwork.\n");
    }

    Rete* rete = (Rete*)malloc(sizeof(Rete));
    if (rete)
    {
        rete->kb = kb;
        rete->rules = rules;
        rete->alphaNodes = createLinkedList();
        rete->betaNodes = createLinkedList();

        // Alpha nodes
        // One alpha node per rule atomic condition
        LinkedListNode* currRule = (LinkedListNode*)rules->first;
        int count = 0;
        while (currRule != NULL)
        {
            Rule* r = (Rule*)currRule->data;
            LinkedListNode* currBody = (LinkedListNode*)r->body->first;
            while (currBody != NULL)
            {
                Fact* a = (Fact*)currBody->data;
                pushLinkedList(rete->alphaNodes, createAlphaNode(a, r));
                count++;
                currBody = currBody->next;
            }

            currRule = currRule->next;
        }

        // Put alpha memories in common for alpha nodes that have similar atomic conditions.

        LinkedListNode* currAlpha = rete->alphaNodes->first;
        while (currAlpha != NULL)
        {
            AlphaNode* alpha = (AlphaNode*)currAlpha->data;
            if (!alpha->shared)
            {
                LinkedListNode* currOtherAlpha = rete->alphaNodes->first;
                while (currOtherAlpha != NULL)
                {
                    AlphaNode* otherAlpha = (AlphaNode*)currOtherAlpha->data;

                    if (alpha != otherAlpha && !otherAlpha->shared && matchTwoAtoms(alpha->atom, otherAlpha->atom))
                    {
                        freeFactLinkedList(otherAlpha->matches);
                        freeFactLinkedList(otherAlpha->newMatches);
                        freeFactLinkedList(otherAlpha->deletedMatches);
                        
                        otherAlpha->matches = alpha->matches;
                        otherAlpha->newMatches = alpha->newMatches;
                        otherAlpha->deletedMatches = alpha->deletedMatches;
                        otherAlpha->shared = true;

                        if (alpha->sharedAlphaNodes == NULL)
                        {
                            alpha->sharedAlphaNodes = createLinkedList();
                        }
                        pushLinkedList(alpha->sharedAlphaNodes, otherAlpha);
                    }
                    
                    currOtherAlpha = currOtherAlpha->next;
                }
            }

            currAlpha = currAlpha->next;
        }

        if (verboseOn)
        {
            printf("Created %d AlphaNodes.\n", count);
        }

        //Beta nodes
        // First b beta node joins the first two alpha nodes of b rule
        // Then b beta node joins the previous one with the following alpha node, etc.
        // until no alpha node remains disconnected.
        currRule = rules->first;
        count = 0;
        while (currRule != NULL)
        {
            Rule* r = (Rule*)currRule->data;
            // Find all alpha nodes by rule
            LinkedList* alpha_r = findAlphaNodesByRule(rete, r);

            if (r->body->size == 1)
            {
                AlphaNode* a = (AlphaNode*) alpha_r->first->data;
                pushLinkedList(rete->betaNodes, createBetaNodeFromAlpha(r, a, NULL));
                count++;
            }
            else if (r->body->size > 1)
            {
                bool first_node = true;
                LinkedListNode* currAlpha = alpha_r->first;
                
                while (currAlpha != NULL)
                {
                    AlphaNode* a = (AlphaNode*)currAlpha->data;
                    if (first_node)
                    {
                        pushLinkedList(rete->betaNodes, createBetaNodeFromAlpha(r, a, (AlphaNode*)currAlpha->next->data));
                        currAlpha = currAlpha->next;
                        first_node = false;
                        count++;
                    }
                    else
                    {
                        pushLinkedList(rete->betaNodes, createBetaNodeFromBeta(r, (BetaNode*)rete->betaNodes->last->data, a));
                        count++;
                    }

                    currAlpha = currAlpha->next;
                }
            }

            freeLinkedList(alpha_r);

            currRule = currRule->next;
        }

        rete->terminalBetaNodes = createLinkedList();
        LinkedListNode* currBeta = rete->betaNodes->first;
        while (currBeta != NULL)
        {
            BetaNode* beta = (BetaNode*)currBeta->data;
            if (beta->terminalNode)
            {
                pushLinkedList(rete->terminalBetaNodes, beta);
                beta->matchingAlphaNodes = rete->alphaNodes;

                // LinkedListNode* currAlpha = rete->alphaNodes->first;
                // while (currAlpha != NULL)
                // {
                //     AlphaNode* alpha = (AlphaNode*)currAlpha->data;
                //     if (!alpha->shared && checkAtomsInclusion(alpha->atom, beta->rule->head))
                //     {
                //         if (beta->matchingAlphaNodes == NULL)
                //         {
                //             beta->matchingAlphaNodes = createLinkedList();
                //         }
                //         pushLinkedList(beta->matchingAlphaNodes, alpha);
                //     }

                //     currAlpha = currAlpha->next;
                // }

            }

            currBeta = currBeta->next;
        }

        if (verboseOn)
        {
            printf("Created %d BetaNodes.\n", count);
        }
    }

    if (verboseOn)
    {
        printf("Leave function createReteNetwork.\n");
    }
    
    return rete;
}

void addExplicitFacts(Rete* rete, SordModel* newFacts)
{
    if (verboseOn)
    {
        printf("Enter function infer.\n");
    }

    SordWorld* world = sord_get_world(rete->kb);

    SordModel* kb = newFacts == NULL ? rete->kb : newFacts;

    int nbFacts = sord_num_quads(rete->kb);
    bool keepInferring = true;
    bool firstIteration = true;

    while (keepInferring)
    {
        LinkedList* triggeredAlphaNodes = createLinkedList();

        // Check all alpha nodes
        LinkedListNode* currAlpha = rete->alphaNodes->first;
        while (currAlpha != NULL)
        {
            AlphaNode* alpha = (AlphaNode*)currAlpha->data;

            if (firstIteration)
            {
                if (!alpha->shared)
                {
                    bool checkNext = checkAlphaNode(alpha, kb);
                    if (checkNext)
                    {
                        pushLinkedList(triggeredAlphaNodes, alpha);
                        alpha->checked = false;
                        alpha->triggered = true;
                        
                        if (alpha->sharedAlphaNodes != NULL)
                        {
                            LinkedListNode* currSharedAlpha = alpha->sharedAlphaNodes->first;
                            while (currSharedAlpha != NULL)
                            {
                                AlphaNode* sharedAlpha = (AlphaNode*)currSharedAlpha->data;
                                sharedAlpha->triggered = true;
                                sharedAlpha->checked = false;
                                sharedAlpha->oldNewMatchesSize = sharedAlpha->newMatches->size;
                                pushLinkedList(triggeredAlphaNodes, sharedAlpha);

                                currSharedAlpha = currSharedAlpha->next;
                            }
                        }
                    }
                }
            }
            else if (alpha->newMatches->size > 0)
            {
                pushLinkedList(triggeredAlphaNodes, alpha);
                alpha->triggered = true;
                alpha->checked = false;
            }
            
            currAlpha = currAlpha->next;
        }

        currAlpha = triggeredAlphaNodes->first;
        while (currAlpha != NULL)
        {
            AlphaNode* alpha = (AlphaNode*)currAlpha->data;
            // printf("Alpha during inf: %d - %zu\n", alpha, alpha->newMatches->size);

            // alpha is the right parent of the following beta node
            if (alpha == alpha->successor->rightPredecessor)
            {
                // the following beta node has two alpha parents
                if (alpha->successor->fromAlpha)
                {
                    checkBetaNodeAddFact(alpha->successor, alpha->successor->alphaLeftPredecessor->matches, alpha->newMatches, alpha->successor->alphaLeftPredecessor->matches->size, alpha->oldNewMatchesSize, NULL, rete->kb);

                    if (alpha->successor->alphaLeftPredecessor->triggered && alpha->successor->alphaLeftPredecessor->checked)
                    {
                        checkBetaNodeAddFact(alpha->successor, alpha->successor->alphaLeftPredecessor->newMatches, alpha->newMatches, alpha->successor->alphaLeftPredecessor->oldNewMatchesSize, alpha->oldNewMatchesSize, NULL, rete->kb);
                    }
                }
                // the following beta node has a beta parent and an alpha parent
                else
                {
                    checkBetaNodeAddFact(alpha->successor, alpha->successor->betaLeftPredecessor->matches, alpha->newMatches, alpha->successor->betaLeftPredecessor->matches->size, alpha->oldNewMatchesSize, NULL, rete->kb);
                }
            }
            // alpha is the left parent of the following beta node
            else
            {
                // the following beta node has two alpha parents
                if (alpha->successor->rightPredecessor != NULL)
                {
                    checkBetaNodeAddFact(alpha->successor, alpha->newMatches, alpha->successor->rightPredecessor->matches, alpha->oldNewMatchesSize, alpha->successor->rightPredecessor->matches->size, NULL, rete->kb);

                    if (alpha->successor->rightPredecessor->triggered && alpha->successor->rightPredecessor->checked)
                    {
                        checkBetaNodeAddFact(alpha->successor, alpha->newMatches, alpha->successor->rightPredecessor->newMatches, alpha->oldNewMatchesSize, alpha->successor->rightPredecessor->oldNewMatchesSize, NULL, rete->kb);
                    }
                }
                // the following beta node has only one alpha parent
                else
                {
                    checkBetaNodeAddFact(alpha->successor, alpha->newMatches, NULL, alpha->oldNewMatchesSize, 0, NULL, rete->kb);
                }
            }

            alpha->checked = true;

            currAlpha = currAlpha->next;
        }

        currAlpha = triggeredAlphaNodes->first;
        while (currAlpha != NULL)
        {
            AlphaNode* alpha = (AlphaNode*)currAlpha->data;
            moveAlphaMatches(alpha);
            alpha->triggered = false;
            currAlpha = currAlpha->next;
        }

        bool foundNewAlphaMatches = false;
        currAlpha = rete->alphaNodes->first;
        while (currAlpha != NULL)
        {
            AlphaNode* alpha = (AlphaNode*)currAlpha->data;
            if (alpha->newMatches->size > 0)
            {
                foundNewAlphaMatches = true;
                break;
            }

            currAlpha = currAlpha->next;
        }

        int nbNewFacts = sord_num_quads(rete->kb);
        // printf("%d new facts\n", nbNewFacts - nbFacts);

        if (nbNewFacts > nbFacts)
        {
            nbFacts = nbNewFacts;
        }
        else if (!foundNewAlphaMatches)
        {
            keepInferring = false;
        }

        firstIteration = false;

        freeLinkedList(triggeredAlphaNodes);
    }

    LinkedListNode* currAlpha = rete->alphaNodes->first;
    while (currAlpha != NULL)
    {
        AlphaNode* alpha = (AlphaNode*)currAlpha->data;
        moveAlphaMatches(alpha);
        currAlpha = currAlpha->next;
    }
}

void removeExplicitFacts(Rete* rete, SordModel* deletedFacts)
{
    int nbFacts = sord_num_quads(rete->kb);
    bool keepInferring = true;
    bool firstIteration = true;

    SordWorld* world = sord_get_world(rete->kb);

    while (keepInferring)
    {
        LinkedList* checkedAlphaNodes = createLinkedList();

        LinkedList* betaNodesToCheck = createLinkedList();

        // Check all alpha nodes
        LinkedListNode* currAlpha = rete->alphaNodes->first;
        while (currAlpha != NULL)
        {
            AlphaNode* alpha = (AlphaNode*)currAlpha->data;
            if (firstIteration)
            {
                if (!alpha->shared)
                {
                    bool checkNext = checkAlphaNodeRemoveFacts(alpha, deletedFacts);
                    if (checkNext)
                    {
                        pushLinkedList(checkedAlphaNodes, alpha);
                        alpha->checked = false;
                        alpha->triggered = true;

                        if (alpha->sharedAlphaNodes != NULL)
                        {
                            LinkedListNode* currSharedAlpha = alpha->sharedAlphaNodes->first;
                            while (currSharedAlpha != NULL)
                            {
                                AlphaNode* sharedAlpha = (AlphaNode*)currSharedAlpha->data;
                                sharedAlpha->triggered = true;
                                sharedAlpha->checked = false;
                                sharedAlpha->oldDeletedMatchesSize = sharedAlpha->deletedMatches->size;
                                pushLinkedList(checkedAlphaNodes, sharedAlpha);

                                currSharedAlpha = currSharedAlpha->next;
                            }
                        }
                    }
                }
            }
            else if (alpha->deletedMatches->size > 0)
            {
                pushLinkedList(checkedAlphaNodes, alpha);
                alpha->checked = false;
                alpha->triggered = true;
            }
            
            currAlpha = currAlpha->next;
        }

        currAlpha = checkedAlphaNodes->first;
        while (currAlpha != NULL)
        {
            AlphaNode* alpha = (AlphaNode*)currAlpha->data;
            bool fromLeft = false;
            if (alpha->successor->fromAlpha && alpha->successor->alphaLeftPredecessor == alpha)
            {
                fromLeft = true;
            }
            
            checkBetaNodeRemoveFact(alpha->successor, fromLeft, rete->kb, rete->terminalBetaNodes);

            alpha->checked = true;

            currAlpha = currAlpha->next;
        }

        for (currAlpha = checkedAlphaNodes->first; currAlpha != NULL; currAlpha = currAlpha->next)
        {
            AlphaNode* alpha = (AlphaNode*)currAlpha->data;
            moveAlphaMatches(alpha);
            alpha->triggered = false;
        }

        bool foundDeletedAlphaMatches = false;
        currAlpha = rete->alphaNodes->first;
        while (currAlpha != NULL)
        {
            AlphaNode* alpha = (AlphaNode*)currAlpha->data;
            if (alpha->deletedMatches->size > 0)
            {
                foundDeletedAlphaMatches = true;
                break;
            }
            currAlpha = currAlpha->next;
        }

        if (!foundDeletedAlphaMatches)
        {
            keepInferring = false;
        }

        freeLinkedList(checkedAlphaNodes);
        freeLinkedList(betaNodesToCheck);

        firstIteration = false;
    }
}

LinkedList* findAlphaNodesByRule(Rete* rete, Rule* r)
{
    LinkedList* res = createLinkedList();
    LinkedListNode* currAlpha = rete->alphaNodes->first;
    while (currAlpha != NULL)
    {
        AlphaNode* a = (AlphaNode*)currAlpha->data;
        if (a->rule == r)
        {
            pushLinkedList(res, a);
        }

        currAlpha = currAlpha->next;
    }

    return res;
}

void cleanRete(Rete* rete)
{
    // Clean alpha nodes
    LinkedListNode* currAlpha = rete->alphaNodes->first;
    while (currAlpha != NULL)
    {
        AlphaNode* a = (AlphaNode*)currAlpha->data;
        if (!a->shared)
        {
            // Delete matches
            FactLinkedListNode* currFact = a->matches->first;
            while (currFact != NULL)
            {
                Fact* f = (Fact*)currFact->data;
                destroyFact(f, false, true);
                currFact = deleteFactCursor(a->matches, currFact);
            }
        }

        currAlpha = currAlpha->next;
    }

    //Clean beta nodes
    LinkedListNode* currBeta = rete->betaNodes->first;
    while (currBeta != NULL)
    {
        BetaNode* b = (BetaNode*)currBeta->data;
        LinkedListNode* currMatch = b->matches->first;
        while (currMatch != NULL)
        {
            BetaMatch* m = (BetaMatch*)currMatch->data;
            destroyBetaMatch(m);
            currMatch = currMatch->next;
        }
        freeLinkedList(b->matches);
        b->matches = createLinkedList();

        if (b->implicitFacts != NULL)
        {
            LinkedListNode* currImpl = b->implicitFacts->first;
            while (currImpl != NULL)
            {
                ImplicitFact* impl = (ImplicitFact*)currImpl->data;
                destroyFact(impl->value, false, false);
                destroyImplicitFact(impl);
                currImpl = currImpl->next;
            }

            freeLinkedList(b->implicitFacts);
            b->implicitFacts = createLinkedList();
        }

        currBeta = currBeta->next;
    }
}

void destroyRete(Rete* rete)
{
    // Clean Rete
    cleanRete(rete);

    // Free alpha nodes
    LinkedListNode* currAlpha = rete->alphaNodes->first;
    while (currAlpha != NULL)
    {
        AlphaNode* a = (AlphaNode*)currAlpha->data;
        if (!a->shared)
        {
            freeFactLinkedList(a->matches);
            freeFactLinkedList(a->newMatches);
            freeFactLinkedList(a->deletedMatches);

            if (a->sharedAlphaNodes != NULL)
            {
                freeLinkedList(a->sharedAlphaNodes);
            }
        }
        
        free(a);
        currAlpha = currAlpha->next;
    }
    freeLinkedList(rete->alphaNodes);
    
    // Free beta nodes
    LinkedListNode* currBeta = rete->betaNodes->first;
    while (currBeta != NULL)
    {
        BetaNode* b = (BetaNode*)currBeta->data;
        freeLinkedList(b->variables->joinVariables);
        freeLinkedList(b->variables->otherVariables);
        free(b->variables);
        freeLinkedList(b->matches);
        freeLinkedList(b->newMatches);
        freeLinkedList(b->deletedMatches);
        if (b->implicitFacts != NULL)
        {
            freeLinkedList(b->implicitFacts);
        }

        // if (b->matchingAlphaNodes != NULL)
        // {
        //     freeLinkedList(b->matchingAlphaNodes);
        // }
        
        free(b);
        currBeta = currBeta->next;
    }
    freeLinkedList(rete->betaNodes);
    freeLinkedList(rete->terminalBetaNodes);

    // Free rules
    LinkedListNode* currRule = rete->rules->first;
    while (currRule != NULL)
    {
        destroyRule((Rule*)currRule->data);
        currRule = currRule->next;
    }
    freeLinkedList(rete->rules);

    // Free rete
    free(rete);
}