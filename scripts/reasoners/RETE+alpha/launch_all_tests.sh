#!/bin/bash

make clean
make

if [ -d "results" ]; then rm -r "results"; fi
mkdir "results"

mkdir "results/RDFS-Simple"
mkdir "results/RDFS-Default"
mkdir "results/RDFS-Full"

./test.sh 20 ../rulesets/rdfs-simple.txt  ../../../dataset-ttl/ results/RDFS-Simple/
./test.sh 20 ../rulesets/rdfs-default.txt  ../../../dataset-ttl/ results/RDFS-Default/
./test.sh 20 ../rulesets/rdfs-full.txt  ../../../dataset-ttl/ results/RDFS-Full/