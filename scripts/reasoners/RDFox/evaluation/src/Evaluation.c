// Copyright 2021 by Oxford Semantic Technologies Limited.

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

#include "CRDFox.h"

void handleException(const CException* exception) {
    if (exception) {
        const char* exceptionName = CException_getExceptionName(exception);
        const char* what = CException_what(exception);
        printf("Exception:\n");
        printf("Name: %s\n", exceptionName);
        printf("What: %s\n", what);
        exit(1);
    }
}

bool stdoutOutputStreamFlush(void* context) {
    int result = fflush(stdout);
    if (result != 0) {
        return false;
    }    
    return true;
}

bool stdoutOutputStreamWrite(void* context, const void* data, size_t numberOfBytesToWrite) {
    size_t bytesWritten = fwrite(data, sizeof(char), numberOfBytesToWrite, stdout);
    if (bytesWritten != numberOfBytesToWrite) {
        return false;
    }
    return true;
}

int main(int argc, char* argv[])
{
    if (argc > 3)
    {
        handleException(CServer_startLocalServer(CParameters_getEmptyParameters()));

        CServer_createFirstLocalServerRole("", "");

        CServerConnection* serverConnection = NULL;
        handleException(CServerConnection_newServerConnection("", "", &serverConnection));

        // We next specify how many threads the server should use during import of data and reasoning.
        // printf("Setting the number of threads...\n");
        handleException(CServerConnection_setNumberOfThreads(serverConnection, 1));
        
        // We the default value for the "type" perameter, which is "par-complex-nn".
        handleException(CServerConnection_createDataStore(serverConnection, "eval", CParameters_getEmptyParameters()));
        
        // We connect to the data store.
        CDataStoreConnection* dataStoreConnection = NULL;
        handleException(CServerConnection_newDataStoreConnection(serverConnection, "eval", &dataStoreConnection));
        
        // We next import the RDF data into the store. At present, only Turtle/N-triples files are supported.
        // At the moment, please convert RDF/XML files into Turtle format to load into CRDFox.
        // printf("Importing RDF data...\n");

        CPrefixes* emptyPrefixes = NULL;
        CPrefixes_newEmptyPrefixes(&emptyPrefixes);

        CPrefixes* defaultPrefixes = NULL;
        CPrefixes_newDefaultPrefixes(&defaultPrefixes);

        handleException(CDataStoreConnection_importDataFromFile(dataStoreConnection, NULL, UPDATE_TYPE_ADDITION, emptyPrefixes, argv[1], ""));

        const CException* exception = CDataStoreConnection_importDataFromFile(dataStoreConnection, NULL, UPDATE_TYPE_ADDITION, emptyPrefixes, argv[2], "text/turtle");
        if (exception) {
            const char* exceptionName = CException_getExceptionName(exception);
            const char* what = CException_what(exception);
            printf("Exception:\n");
            printf("Name: %s\n", exceptionName);
            printf("What: %s\n", what);
        }

        exception = CDataStoreConnection_importDataFromFile(dataStoreConnection, NULL, UPDATE_TYPE_ADDITION, emptyPrefixes, argv[3], "text/turtle");
        if (exception) {
            const char* exceptionName = CException_getExceptionName(exception);
            const char* what = CException_what(exception);
            printf("Exception:\n");
            printf("Name: %s\n", exceptionName);
            printf("What: %s\n", what);
        }

        // RDFox manages data in several domains.
        //
        // - EDB are the explicitly stated facts.
        //
        // - IDB facts are the EDB facts plus all of their consequences. This is what normally should be
        //   queried -- that is, these are the "current" facts in the store.
        //
        // - IDBrep is different from IDB only if optimized equality reasoning is used. In that case, RDFox
        //   will select for each set of equal resources one representative, and IDBrep then consists of the
        //   IDB facts that contain just the representative resources.
        //
        // - IDBrepNoEDB is equal IDBrep minus EDB.
        //
        // The domain must be specified in various places where queries are evaluated. If a query domain is not
        // specified, the IDB domain is used.
        // printf("Number of tuples after import: %zu\n", getTriplesCount(dataStoreConnection, "IDB", emptyPrefixes));
        
        // SPARQL queries can be evaluated in several ways. One option is to have the query result be written to
        // an output stream in one of the supported formats.

        // We now add the ontology and the custom rules to the data.
        
        // In this example, the rules are kept in a file separate from the ontology. CRDFox supports
        // SWRL rules; thus, it is possible to store the rules into the OWL ontology. However, CRDFox
        // does not (yet) support SWRL built-in predicates, so any rules involving built-in predicates
        // should be written in the native format of RDFox. The format of the rules should be obvious
        // from the example. Built-in functions are invoked using the BIND and FILTER syntax from
        // SPARQL, and most SPARQL built-in functions are supported.
    

        // printf("Importing rules from a file...\n");
        

        // printf("Number of tuples after materialization: %zu\n", getTriplesCount(dataStoreConnection, "IDB", emptyPrefixes));
        
        // Since the iterator is exhausted, it does not need to be closed.
        // printf("---------------------------------------------------------------------------------------\n");
        // printf("  The number of rows returned: %d\n", numberOfRows);
        // printf("=======================================================================================\n\n");
        

        // RDFox supports incremental reasoning. One can import facts into the store incrementally by
        // calling importDataFromFileName(dataStoreConnection, ...) with additional argument UPDATE_TYPE_ADDITION.
        // printf("Import triples for incremental reasoning\n");

        if (argc > 5)
        {
            if (strcmp(argv[4], "add") == 0)
            {
                for (int i = 5; i < argc; i++)
                {
                    CDataStoreConnection_importDataFromFile(dataStoreConnection, NULL, UPDATE_TYPE_ADDITION, defaultPrefixes, argv[i], "text/turtle");
                }
            }
            else if (strcmp(argv[4], "delete") == 0)
            {
                for (int i = 5; i < argc; i++)
                {
                    CDataStoreConnection_importDataFromFile(dataStoreConnection, NULL, UPDATE_TYPE_DELETION, defaultPrefixes, argv[i], "text/turtle");
                }
            }
            else if (strcmp(argv[4], "add-delete") == 0)
            {
                for (int i = 5; i < argc; i++)
                {
                    CDataStoreConnection_importDataFromFile(dataStoreConnection, NULL, UPDATE_TYPE_ADDITION, defaultPrefixes, argv[i], "text/turtle");
                    CDataStoreConnection_importDataFromFile(dataStoreConnection, NULL, UPDATE_TYPE_DELETION, defaultPrefixes, argv[i], "text/turtle");
                }
            }
        }

        CDataStoreConnection_destroy(dataStoreConnection);
    }
    
    return 0;
}
