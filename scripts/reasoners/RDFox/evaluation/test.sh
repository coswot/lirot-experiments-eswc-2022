#!/bin/bash

NB_TESTS=$1
RULESET=$2
DATASET_DIR=$3
RESULTS_DIR=$4

echo "Full Materialization tests."

for INPUT in "$DATASET_DIR"*
do
    if [[ -d $INPUT ]]
    then
        IFS='/' read -r -a PARTS <<< "$INPUT"
        IFS='.' read -r -a SIZE <<< "${PARTS[-1]}"

        mkdir $RESULTS_DIR$SIZE

        FILENAME="$INPUT/full.ttl"
        echo "--------------- $SIZE ----------------"
        RESULT_FILENAME="$RESULTS_DIR$SIZE/full.csv"
        if [ -d "$RESULT_FILENAME" ]; then rm "$RESULT_FILENAME"; fi
        touch $RESULT_FILENAME
        echo "real_time;user_time;sys_time;max_rss;max_vm" > $RESULT_FILENAME

        for (( c=1; c<=$NB_TESTS; c++ ))
        do
            sudo LD_LIBRARY_PATH=../lib ./tstime/tstime -t ./Evaluation $RULESET "$DATASET_DIR"univ-bench.ttl $FILENAME &>> $RESULT_FILENAME
            echo "Test $c/$NB_TESTS"
        done
    fi
done

echo "Incremental insertions."

for INPUT in "$DATASET_DIR"*
do
    if [[ -d $INPUT ]]
    then
        IFS='/' read -r -a PARTS <<< "$INPUT"
        IFS='.' read -r -a SIZE <<< "${PARTS[-1]}"

        INIT="$INPUT/init.ttl"
        echo "--------------- $SIZE ----------------"
        RESULT_FILENAME="$RESULTS_DIR$SIZE/add.csv"
        if [ -d "$RESULT_FILENAME" ]; then rm "$RESULT_FILENAME"; fi
        touch $RESULT_FILENAME
        echo "real_time;user_time;sys_time;max_rss;max_vm" > $RESULT_FILENAME

        ADD_FILENAMES=""
        for INPUT in "$DATASET_DIR$SIZE/parts/"*
        do
            ADD_FILENAMES="$ADD_FILENAMES $INPUT"
        done

        for (( c=1; c<=$NB_TESTS; c++ ))
        do
            sudo ./tstime/tstime -t ./bin/reasoner $RULESET "$DATASET_DIR"univ-bench.ttl $INIT add $ADD_FILENAMES &>> $RESULT_FILENAME
            sudo LD_LIBRARY_PATH=../lib ./tstime/tstime -t ./Evaluation $RULESET "$DATASET_DIR"univ-bench.ttl $INIT add $ADD_FILENAMES &>> $RESULT_FILENAME
            echo "Test $c/$NB_TESTS"
        done
    fi
done

echo "Incremental deletions."

for INPUT in "$DATASET_DIR"*
do
    if [[ -d $INPUT ]]
    then
        IFS='/' read -r -a PARTS <<< "$INPUT"
        IFS='.' read -r -a SIZE <<< "${PARTS[-1]}"

        INIT="$INPUT/full.ttl"
        echo "--------------- $SIZE ----------------"
        RESULT_FILENAME="$RESULTS_DIR$SIZE/delete.csv"
        if [ -d "$RESULT_FILENAME" ]; then rm "$RESULT_FILENAME"; fi
        touch $RESULT_FILENAME
        echo "real_time;user_time;sys_time;max_rss;max_vm" > $RESULT_FILENAME

        DELETE_FILENAMES=""
        for INPUT in "$DATASET_DIR$SIZE/parts/"*
        do
            DELETE_FILENAMES="$DELETE_FILENAMES $INPUT"
        done

        for (( c=1; c<=$NB_TESTS; c++ ))
        do
            sudo LD_LIBRARY_PATH=../lib ./tstime/tstime -t ./Evaluation $RULESET "$DATASET_DIR"univ-bench.ttl $INIT delete $DELETE_FILENAMES &>> $RESULT_FILENAME
            echo "Test $c/$NB_TESTS"
        done
    fi
done

echo "Successive insertions and deletions."

for INPUT in "$DATASET_DIR"*
do
    if [[ -d $INPUT ]]
    then
        IFS='/' read -r -a PARTS <<< "$INPUT"
        IFS='.' read -r -a SIZE <<< "${PARTS[-1]}"

        INIT="$INPUT/init.ttl"
        echo "--------------- $SIZE ----------------"
        RESULT_FILENAME="$RESULTS_DIR$SIZE/add-delete.csv"
        if [ -d "$RESULT_FILENAME" ]; then rm "$RESULT_FILENAME"; fi
        touch $RESULT_FILENAME
        echo "real_time;user_time;sys_time;max_rss;max_vm" > $RESULT_FILENAME

        ADD_FILENAMES=""
        for INPUT in "$DATASET_DIR$SIZE/parts/"*
        do
            ADD_FILENAMES="$ADD_FILENAMES $INPUT"
        done

        for (( c=1; c<=$NB_TESTS; c++ ))
        do
            sudo LD_LIBRARY_PATH=../lib ./tstime/tstime -t ./Evaluation $RULESET "$DATASET_DIR"univ-bench.ttl $INIT add-delete $ADD_FILENAMES &>> $RESULT_FILENAME
            echo "Test $c/$NB_TESTS"
        done
    fi
done
