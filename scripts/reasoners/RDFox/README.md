# Test scripts for RDFox

This folder contains the scripts that were used to run experimentations with RDFox.

## Setup

To setup the script, first download the [latest version of RDFox](https://www.oxfordsemantic.tech/downloads) (we used version 5.4 for our experimentation) and request an [evaluation licence](https://www.oxfordsemantic.tech/tryrdfoxforfree). The licence may take a few days to be accepted and sent by email.

Once the licence is available, move the ```include``` and ```lib``` folders from the downloaded RDFox archive to this current directory (```scripts/reasoners/RDFox```). Then place your licence (```RDFox.lic```) in the ```evaluation``` folder.

Then, in the current directory run the following commands:

```bash
git submodule init
git submodule update
cd evaluation/tstime
make
cd ..
chmod +x build.sh
chmod +x test.sh
chmod +x launch_all_tests.sh
./build.sh
```

RDFox is now ready to use.

## Run the scripts

To run the tests, after completing the setup step, in the ```evaluation``` folder, execute the following command:

```bash
./launch_all_tests.sh
```

As all tests are run at once, this command may take a lot of time to execute.
