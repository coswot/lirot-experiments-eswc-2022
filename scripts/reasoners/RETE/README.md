# Test scripts for RETE

This folder contains the scripts that were used to run experimentations with RETE.

## Setup

To setup the scripts, in this directory run the following commands:

```bash
git submodule init
git submodule update
cd tstime
make
cd ..
chmod +x test.sh
chmod +x launch_all_tests.sh
```

## Run the scripts

To run the tests, after completing the setup step, execute the following command:

```bash
./launch_all_tests.sh
```

As all tests are run at once, this command may take a lot of time to execute.
