#include "AlphaNode.h"

bool checkAlphaNode(AlphaNode* alpha, SordModel* kb)
{
    /*
    * For each fact, check if it matches with the node's atomic condition.
    * A match can occur if b fact checks all litteral parts of the atom,
    * or if the atom has variables and all the instances of the variables are checked.
    */

    if (verboseOn)
    {
        printf("Enter function checkAlphaNode.\n");
    }

    bool checkNext = false;

    SordNode* subject = NULL;
    SordNode* predicate = NULL;
    SordNode* object = NULL;

    SordWorld* world = sord_get_world(kb);

    if (alpha->atom->subject->type != VARIABLE)
    {
        subject = getSordNodeFromTerm(alpha->atom->subject, world);
    }
    if (alpha->atom->predicate->type != VARIABLE)
    {
        predicate = getSordNodeFromTerm(alpha->atom->predicate, world);
    }
    if (alpha->atom->object->type != VARIABLE)
    {
        object = getSordNodeFromTerm(alpha->atom->object, world);
    }

    SordQuad pattern = {subject, predicate, object, NULL};

    SordIter* matches = sord_find(kb, pattern);

    if (subject != NULL)
    {
        sord_node_free(world, subject);
    }
    if (predicate != NULL)
    {
        sord_node_free(world, predicate);
    }
    if (object != NULL)
    {
        sord_node_free(world, object);
    }

    while (!sord_iter_end(matches))
    {
        SordQuad quad;
        sord_iter_get(matches, quad);

        Fact* f = createFact(getTermFromSordNode(quad[0], false),
                             getTermFromSordNode(quad[1], false),
                             getTermFromSordNode(quad[2], false));
        
        addFact(alpha->newMatches, f);
        checkNext = true;

        sord_iter_next(matches);
    }

    alpha->oldNewMatchesSize = alpha->newMatches->size;

    sord_iter_free(matches);

    if (verboseOn)
    {
        printf("Leave function checkAlphaNode.\n");
    }

    return checkNext;
}

bool alphaNodeMatchFact(AlphaNode* alpha, Fact* f)
{
    bool match = true;

    // Check subject
    if (alpha->atom->subject->type != VARIABLE && strcmp(alpha->atom->subject->value, f->subject->value) != 0)
    {
        match = false;
    }
    if (match && alpha->atom->predicate->type != VARIABLE && strcmp(alpha->atom->predicate->value, f->predicate->value) != 0)
    {
        match = false;
    }
    if (match && alpha->atom->object->type != VARIABLE && strcmp(alpha->atom->object->value, f->object->value) != 0)
    {
        match = false;
    }

    return match;
}

// char* getResourceValue(char* resource, SordModel* kb)
// {
//     SordWorld* world = sord_get_world(kb);
//     SordNode* subject = sord_new_uri(world, (const uint8_t*)resource);
//     SordNode* predicate = sord_new_uri(world, (const uint8_t*)"http://www.w3.org/1999/02/22-rdf-syntax-ns#value");

//     SordQuad pattern = {subject, predicate, NULL, NULL};
//     SordIter* matches = sord_find(kb, pattern);

//     char* result = NULL;

//     if (matches != NULL)
//     {
//         SordQuad valueRes;
//         sord_iter_get(matches, valueRes);
//         if (sord_node_get_type(valueRes[2]) == SORD_LITERAL)
//         {
//             result = (char*)sord_node_get_string(valueRes[2]);
//         }

//         sord_iter_free(matches);
//     }
    
//     return result;
// }

AlphaNode* createAlphaNode(Fact* atom, Rule* rule)
{
    AlphaNode* result = (AlphaNode*)malloc(sizeof(AlphaNode));
    if (result)
    {
        result->atom = atom;
        result->rule = rule;
        result->matches = createFactLinkedList();
        result->newMatches = createFactLinkedList();
        result->deletedMatches = createFactLinkedList();
        result->oldNewMatchesSize = 0;
        result->oldDeletedMatchesSize = 0;
    }

    if (verboseOn)
    {
        printf("Created AlphaNode with condition (%s %s %s).\n",
            result->atom->subject->value, result->atom->predicate->value, result->atom->object->value);
    }
    
    return result;
}

void moveAlphaMatches(AlphaNode* alpha)
{
    // Move new matches
    FactLinkedListNode* curr = alpha->newMatches->first;
    int count = 0;
    while (curr != NULL && count < alpha->oldNewMatchesSize)
    {
        count++;
        Fact* f = (Fact*)curr->data;
        addFact(alpha->matches, f);
        curr = deleteFactCursor(alpha->newMatches, curr);
    }

    alpha->oldNewMatchesSize = alpha->newMatches->size;

    // Free deleted matches
    curr = alpha->deletedMatches->first;
    count = 0;
    while (curr != NULL && count < alpha->oldDeletedMatchesSize)
    {
        count++;
        destroyFact((Fact*)curr->data, false, true);
        curr = deleteFactCursor(alpha->deletedMatches, curr);
    }

    alpha->oldDeletedMatchesSize = alpha->deletedMatches->size;
    // printf("%zu alpha\n", alpha->oldDeletedMatchesSize);
}

bool checkAlphaNodeRemoveFacts(AlphaNode* alpha, SordModel* kb)
{
    bool checkNext = false;

    SordNode* subject = NULL;
    SordNode* predicate = NULL;
    SordNode* object = NULL;

    SordWorld* world = sord_get_world(kb);

    if (alpha->atom->subject->type != VARIABLE)
    {
        subject = getSordNodeFromTerm(alpha->atom->subject, world);
    }
    if (alpha->atom->predicate->type != VARIABLE)
    {
        predicate = getSordNodeFromTerm(alpha->atom->predicate, world);
    }
    if (alpha->atom->object->type != VARIABLE)
    {
        object = getSordNodeFromTerm(alpha->atom->object, world);
    }

    SordQuad pattern = {subject, predicate, object, NULL};

    SordIter* matches = sord_find(kb, pattern);

    if (subject != NULL)
    {
        sord_node_free(world, subject);
    }
    if (predicate != NULL)
    {
        sord_node_free(world, predicate);
    }
    if (object != NULL)
    {
        sord_node_free(world, object);
    }

    while (!sord_iter_end(matches))
    {
        SordQuad quad;
        sord_iter_get(matches, quad);

        Fact* f = createFact(getTermFromSordNode(quad[0], false),
                             getTermFromSordNode(quad[1], false),
                             getTermFromSordNode(quad[2], false));

        // printf("ALPHA DELETE ");
        // printFact(f);
        
        Fact* factToDelete;
        
        for (FactLinkedListNode* currMatch = alpha->matches->first; currMatch != NULL;)
        {
            Fact* match = (Fact*)currMatch->data;
            if (factEquals(match, f))
            {
                currMatch = deleteFactCursor(alpha->matches, currMatch);
                factToDelete = match;
                break;
            }
            else
            {
                currMatch = currMatch->next;
            }
        }
        
        addFact(alpha->deletedMatches, factToDelete);
        destroyFact(f, false, true);
        checkNext = true;

        sord_iter_next(matches);
    }

    alpha->oldDeletedMatchesSize = alpha->deletedMatches->size;
    // printf("%zu alpha to delete\n", alpha->deletedMatches->size);

    sord_iter_free(matches);

    if (verboseOn)
    {
        printf("Leave function checkAlphaNode.\n");
    }

    return checkNext;
}