#include "KBWrapper.h"
#include "KnowledgeBase.h"
#include "Verbose.h"
#include "string.h"

int main(int argc, char* argv[])
{
    detectVerbose(argc, argv);
    
    if (argc > 3)
    {
        // Init KB wrapper with initial facts and rules
        KBWrapper* wrapper = APIInitKBWrapper(argv[2], argv[1]);
        APIAddFactsFromFile(wrapper, argv[3]);

        if (argc > 5)
        {
            if (strcmp(argv[4], "add") == 0)
            {
                for (int i = 5; i < argc; i++)
                {
                    APIAddFactsFromFile(wrapper, argv[i]);
                }
            }
            else if (strcmp(argv[4], "delete") == 0)
            {
                for (int i = 5; i < argc; i++)
                {
                    APIDeleteFactsFromFile(wrapper, argv[i]);
                }
            }
            else if (strcmp(argv[4], "add-delete") == 0)
            {
                for (int i = 5; i < argc; i++)
                {
                    APIAddFactsFromFile(wrapper, argv[i]);
                    APIDeleteFactsFromFile(wrapper, argv[i]);
                }
            }
        }

        // Destroy wrapper
        APIDestroyKBWrapper(wrapper);
    }
    else
    {
        printf("No input file provided.\n");
    }

	return 0;
}
