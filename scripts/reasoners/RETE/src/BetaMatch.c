#include "BetaMatch.h"

BetaMatch* createBetaMatch(int nbVariables)
{
    BetaMatch* m = (BetaMatch*)malloc(sizeof(BetaMatch));
    {
        if (m)
        {
            m->nb_variables = nbVariables;
            m->variables = (Term**)malloc(m->nb_variables * sizeof(Term*));
            if (m->variables)
            {
                // Init variables              
                for (int i = 0; i < m->nb_variables; i++)
                {
                    m->variables[i] = NULL;
                }
            }
        }

        m->leftFactCause = NULL;
        m->leftMatchCause = NULL;
    }

    return m;
}

bool betaMatchEquals(BetaMatch* m1, BetaMatch* m2)
{
    bool result = true;

    if (m1->nb_variables != m2->nb_variables)
    {
        result = false;
    }
    else
    {
        for (int i = 0; i < m1->nb_variables; i++)
        {
            if (!termEquals(m1->variables[i], m2->variables[i]))
            {
                result = false;
                break;
            }
        }
    }

    return result;
}

bool betaMatchContainsOrigin(BetaMatch* m, Fact* f)
{
    bool res = false;

    if (m->rightCause)
    {
        res = factEquals(f, m->rightCause);
    }

    if (!res)
    {
        if (m->leftFactCause != NULL)
        {
            res = factEquals(m->leftFactCause, f);
        }
        else
        {
            res = betaMatchContainsOrigin(m->leftMatchCause, f);
        }
    }

    return res;
}

void destroyBetaMatch(BetaMatch* m)
{
    free(m->variables);
    free(m);
}