#!/usr/bin/python

import numpy as np
import pandas as pd
from sys import argv
from os import listdir
from os.path import isfile, join

OPERATION = argv[1]
INPUT_DIR = argv[2]
OUTPUT_FILE = argv[3]

def compute_mean(data, confidence=0.90):
    data_stripped = sorted(data)
    nb_stripped = round(len(data) * (1 - confidence) / 2)
    data_stripped = data_stripped[nb_stripped:len(data) - nb_stripped]
    a = 1.0 * np.array(data_stripped)
    m = np.mean(a)
    return round(m)

dataset_sizes = []
reasoners = []

for r in [d for d in listdir(INPUT_DIR) if not isfile(join(INPUT_DIR, d))]:
    reasoners = reasoners + [r]
    for s in [int(s) for s in listdir(join(INPUT_DIR, r)) if not isfile(join(join(INPUT_DIR, r), s))]:
        dataset_sizes = dataset_sizes + [s]

reasoners = sorted(reasoners)
dataset_sizes = sorted(set(dataset_sizes))

columns = list(pd.read_csv(join(join(join(INPUT_DIR, reasoners[0]), str(dataset_sizes[0])), OPERATION + ".csv")).columns)

columns = ["reasoner", "dataset-size"] + columns

df = pd.DataFrame(columns=columns)

for r in reasoners:
    for s in dataset_sizes:
        filename = join(join(join(INPUT_DIR, r), str(s)), OPERATION + ".csv")
        values = pd.read_csv(filename)
        row = {}
        row["reasoner"] = r
        row["dataset-size"] = s
        for c in values.columns:
            avg = compute_mean(values[c].to_list())
            row[c] = avg
        df = df.append(row, ignore_index=True)

df.to_csv(OUTPUT_FILE, index=False)
