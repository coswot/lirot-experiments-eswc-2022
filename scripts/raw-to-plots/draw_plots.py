#!/usr/bin/python

from matplotlib import pyplot as plt
import matplotlib
import pandas as pd
from sys import argv

INPUT_FILE = argv[1]
OUTPUT_FILE_1 = argv[2]
OUTPUT_FILE_2 = argv[3]



df = pd.read_csv(INPUT_FILE)

reasoners = sorted(set(df["reasoner"]))
if "--reasoners=all" in argv:
    reasoners = [r for r in reasoners if not r.startswith("RETE")]
elif "--reasoners=rete" in argv:
    reasoners = [r for r in reasoners if r.startswith("RETE") or r == "LiRoT"]

sizes = sorted(set(df["dataset-size"]))

rss_data = {}
time_data = {}

for index, row in df.iterrows():
    r = df["reasoner"].iloc[index]
    if r in reasoners:
        s = df["dataset-size"].iloc[index]
        time = round(int(df["real_time"].iloc[index]) / 1000)

        rss = df["max_rss"].iloc[index] / 1000

        # rss
        if r in rss_data:
            rss_data[r]["avg"] = rss_data[r]["avg"] + [rss]
        else:
            rss_data[r] = {}
            rss_data[r]["avg"] = [rss]
        
        #time
        if r in time_data:
            time_data[r]["avg"] = time_data[r]["avg"] + [time]
        else:
            time_data[r] = {}
            time_data[r]["avg"] = [time]


matplotlib.rcParams.update({'font.size': 30})

fig, (ax_rss, ax_time) = plt.subplots(1, 2, figsize=(25, 10))

styles = pd.read_csv("plot-styles.csv")

for r in reasoners:
    s = styles.loc[styles["reasoner"] == r, "style"].iloc[0]
    color = styles.loc[styles["reasoner"] == r, "color"].iloc[0]
    ax_rss.plot(sizes, rss_data[r]["avg"], s, label=r, linewidth=4, markersize=10, color=color)
    ax_time.plot(sizes, time_data[r]["avg"], s, linewidth=4, markersize=10, color=color)

# RSS
if "--log" in argv:
    ax_rss.set_yscale("log")
    ax_rss.set_xscale("log")

ax_rss.set_title("Maximum resident set size", size=40)
ax_rss.set_ylabel("maxRSS (MB)")
ax_rss.set_xlabel("# explicit triples")
ax_rss.grid(which="major", linewidth=2)
ax_rss.grid(which="minor", linewidth=1)
ax_rss.set_xlim([0, sizes[-1]])
ax_rss.set_ylim(bottom=0)

# Time
if "--log" in argv:
    ax_time.set_yscale("log")
    ax_time.set_xscale("log")

ax_time.set_title("Execution time", size=40)
ax_time.set_ylabel("Execution time (ms)")
ax_time.set_xlabel("# explicit triples")
ax_time.grid(which="major", linewidth=2)
ax_time.grid(which="minor", linewidth=1)
ax_time.set_xlim([0, sizes[-1]])
ax_time.set_ylim(bottom=0)

fig.legend(loc="upper center",   # Position of legend
           borderaxespad=0.2,    # Small spacing around legend box
           bbox_to_anchor=(0.5, 0.0))
fig.tight_layout(pad=3.0)

plt.savefig(OUTPUT_FILE_1, format="eps", bbox_inches='tight')
plt.savefig(OUTPUT_FILE_2, format="png", bbox_inches='tight')
# plt.show()