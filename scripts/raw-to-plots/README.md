# raw-to-plots

This folder contains scripts to generate plots from raw results files.

## Setup

To run theses scripts, you need Python (>= 3.5) and some libraries. First, install Python and the ```pip``` command using the standard package installation procedure on your Linux distribution. On Ubuntu, one can use the following commands:

```bash
sudo apt-get install python3
sudo apt-get install python3-pip
```

Then use ```pip``` to install necessary libraries to run the scripts:

```bash
pip install numpy pandas matplotlib
```

## Scripts

This section presents the scripts used to generate plots. To run them all directly, see section ```draw_all_plots.sh```.

### compute_statistics.py

This Python script aggregates raw results files into single statistics files. To use it, use the following command:

```bash
python compute_statistics.py <test-type> <results-directory> <output-file>
```

Where:

 - ```<test-type>``` is either ```full```, ```add```, ```delete``` or ```add-delete``` (more details [here](https://gitlab.com/coswot/lirot-experiments-eswc-2022/-/blob/main/README.md));
 - ```<results-directory>``` is the path to the directory that contains all raw results for a single ruleset; it must respect the structure described [here](https://gitlab.com/coswot/lirot-experiments-eswc-2022/-/tree/main/results-csv);
 - ```<output-file>``` is the path to the CSV file that will contain the aggregated results.

Here is an example of a call to this script:

```bash
python compute_statistics.py add results-csv/RDFS-Default/ rdfs-default-add.csv
```

This command generates an aggregated results files that contains all test data for the RDFS-Default ruleset, for an ```add``` type test.

### draw_plots.py

This scripts draws plots from a file containing aggregated results (obtained with the ```compute_statistics.py``` script). To use it, use the following command:

```bash
python draw_plots.py <input-file> <eps-output-file> <png-output-file> reasoners=<rete|all> [--log]
```

Where:

 - ```<input-file>``` is an aggregated results file;
 - ```<eps-output-file>``` is the path to a vectorial image that will contain the plots (in EPS format);
 - ```<png-output-file>``` is the path to a PNG image that will contain the plots;
 - ```reasoners``` is the type of comparison to draw (either ```all```: LiRoT vs Jena vs RDFox, or ```rete```: RETE vs RETE+alpha vs RETE+terms vs LiRoT);
 - ```--log``` is an optional argument; if added, horizontal and vertical axes will use a logarithmic scale.

Here are examples of calls to this script:

```bash
python draw_plots.py results-csv/RDFS-Default/add.csv plot_add_rete.eps plot_add_rete.png --reasoners=rete
python draw_plots.py results-csv/RDFS-Default/add.csv plot_add_all.eps plot_add_all.png --reasoners=all --log
```

The first (resp. second) command draws plots for the RDFS-Default ruleset, for the ```add``` type test, to compare all versions of RETE (resp. LiRoT vs Jena vs RDFox).

### draw_all_plots.sh

This shell script runs all Python scripts at once and generates all plots. To use it, if it does not own execution rights, run the following command:

```chmod +x draw_all_plots.sh```

Then in this directory, run the following command:

```./draw_all_plots.sh```

This will create an ```aggregated-results``` directory, that will contain all compiled results in CSV format for each ruleset and for each type of test, then this will create a ```plots``` folder containing all plots.
