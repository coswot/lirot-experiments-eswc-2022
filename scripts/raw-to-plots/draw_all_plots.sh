#!/bin/bash

# Generate all aggregated results files

if [ -d "aggregated-results" ]; then rm -r "aggregated-results"; fi
mkdir "aggregated-results"

mkdir "aggregated-results/RDFS-Simple"
mkdir "aggregated-results/RDFS-Default"
mkdir "aggregated-results/RDFS-Full"

python compute_statistics.py full ../../results-csv/RDFS-Simple/ aggregated-results/RDFS-Simple/full.csv
python compute_statistics.py add ../../results-csv/RDFS-Simple/ aggregated-results/RDFS-Simple/add.csv
python compute_statistics.py delete ../../results-csv/RDFS-Simple/ aggregated-results/RDFS-Simple/delete.csv
python compute_statistics.py add-delete ../../results-csv/RDFS-Simple/ aggregated-results/RDFS-Simple/add-delete.csv

python compute_statistics.py full ../../results-csv/RDFS-Default/ aggregated-results/RDFS-Default/full.csv
python compute_statistics.py add ../../results-csv/RDFS-Default/ aggregated-results/RDFS-Default/add.csv
python compute_statistics.py delete ../../results-csv/RDFS-Default/ aggregated-results/RDFS-Default/delete.csv
python compute_statistics.py add-delete ../../results-csv/RDFS-Default/ aggregated-results/RDFS-Default/add-delete.csv

python compute_statistics.py full ../../results-csv/RDFS-Full/ aggregated-results/RDFS-Full/full.csv
python compute_statistics.py add ../../results-csv/RDFS-Full/ aggregated-results/RDFS-Full/add.csv
python compute_statistics.py delete ../../results-csv/RDFS-Full/ aggregated-results/RDFS-Full/delete.csv
python compute_statistics.py add-delete ../../results-csv/RDFS-Full/ aggregated-results/RDFS-Full/add-delete.csv

# Generate all plots

if [ -d "plots" ]; then rm -r "plots"; fi
mkdir "plots"

mkdir "plots/RDFS-Simple"
mkdir "plots/RDFS-Default"
mkdir "plots/RDFS-Full"

mkdir "plots/RDFS-Simple/All versions of RETE"
mkdir "plots/RDFS-Simple/LiRoT vs Jena vs RDFox"

mkdir "plots/RDFS-Default/All versions of RETE"
mkdir "plots/RDFS-Default/LiRoT vs Jena vs RDFox"

mkdir "plots/RDFS-Full/All versions of RETE"
mkdir "plots/RDFS-Full/LiRoT vs Jena vs RDFox"

mkdir "plots/RDFS-Simple/All versions of RETE/EPS"
mkdir "plots/RDFS-Simple/All versions of RETE/PNG"

mkdir "plots/RDFS-Simple/LiRoT vs Jena vs RDFox/EPS"
mkdir "plots/RDFS-Simple/LiRoT vs Jena vs RDFox/PNG"

mkdir "plots/RDFS-Default/All versions of RETE/EPS"
mkdir "plots/RDFS-Default/All versions of RETE/PNG"

mkdir "plots/RDFS-Default/LiRoT vs Jena vs RDFox/EPS"
mkdir "plots/RDFS-Default/LiRoT vs Jena vs RDFox/PNG"

mkdir "plots/RDFS-Full/All versions of RETE/EPS"
mkdir "plots/RDFS-Full/All versions of RETE/PNG"

mkdir "plots/RDFS-Full/LiRoT vs Jena vs RDFox/EPS"
mkdir "plots/RDFS-Full/LiRoT vs Jena vs RDFox/PNG"

# RDFS-Simple

python draw_plots.py aggregated-results/RDFS-Simple/full.csv "plots/RDFS-Simple/All versions of RETE/EPS/full.eps" "plots/RDFS-Simple/All versions of RETE/PNG/full.png" --reasoners=rete
python draw_plots.py aggregated-results/RDFS-Simple/full.csv "plots/RDFS-Simple/LiRoT vs Jena vs RDFox/EPS/full.eps" "plots/RDFS-Simple/LiRoT vs Jena vs RDFox/PNG/full.png" --reasoners=all --log

python draw_plots.py aggregated-results/RDFS-Simple/add.csv "plots/RDFS-Simple/All versions of RETE/EPS/add.eps" "plots/RDFS-Simple/All versions of RETE/PNG/add.png" --reasoners=rete
python draw_plots.py aggregated-results/RDFS-Simple/add.csv "plots/RDFS-Simple/LiRoT vs Jena vs RDFox/EPS/add.eps" "plots/RDFS-Simple/LiRoT vs Jena vs RDFox/PNG/add.png" --reasoners=all --log

python draw_plots.py aggregated-results/RDFS-Simple/delete.csv "plots/RDFS-Simple/All versions of RETE/EPS/delete.eps" "plots/RDFS-Simple/All versions of RETE/PNG/delete.png" --reasoners=rete
python draw_plots.py aggregated-results/RDFS-Simple/delete.csv "plots/RDFS-Simple/LiRoT vs Jena vs RDFox/EPS/delete.eps" "plots/RDFS-Simple/LiRoT vs Jena vs RDFox/PNG/delete.png" --reasoners=all --log

python draw_plots.py aggregated-results/RDFS-Simple/add-delete.csv "plots/RDFS-Simple/All versions of RETE/EPS/add-delete.eps" "plots/RDFS-Simple/All versions of RETE/PNG/add-delete.png" --reasoners=rete
python draw_plots.py aggregated-results/RDFS-Simple/add-delete.csv "plots/RDFS-Simple/LiRoT vs Jena vs RDFox/EPS/add-delete.eps" "plots/RDFS-Simple/LiRoT vs Jena vs RDFox/PNG/add-delete.png" --reasoners=all --log

# RDFS-Default

python draw_plots.py aggregated-results/RDFS-Default/full.csv "plots/RDFS-Default/All versions of RETE/EPS/full.eps" "plots/RDFS-Default/All versions of RETE/PNG/full.png" --reasoners=rete
python draw_plots.py aggregated-results/RDFS-Default/full.csv "plots/RDFS-Default/LiRoT vs Jena vs RDFox/EPS/full.eps" "plots/RDFS-Default/LiRoT vs Jena vs RDFox/PNG/full.png" --reasoners=all --log

python draw_plots.py aggregated-results/RDFS-Default/add.csv "plots/RDFS-Default/All versions of RETE/EPS/add.eps" "plots/RDFS-Default/All versions of RETE/PNG/add.png" --reasoners=rete
python draw_plots.py aggregated-results/RDFS-Default/add.csv "plots/RDFS-Default/LiRoT vs Jena vs RDFox/EPS/add.eps" "plots/RDFS-Default/LiRoT vs Jena vs RDFox/PNG/add.png" --reasoners=all --log

python draw_plots.py aggregated-results/RDFS-Default/delete.csv "plots/RDFS-Default/All versions of RETE/EPS/delete.eps" "plots/RDFS-Default/All versions of RETE/PNG/delete.png" --reasoners=rete
python draw_plots.py aggregated-results/RDFS-Default/delete.csv "plots/RDFS-Default/LiRoT vs Jena vs RDFox/EPS/delete.eps" "plots/RDFS-Default/LiRoT vs Jena vs RDFox/PNG/delete.png" --reasoners=all --log

python draw_plots.py aggregated-results/RDFS-Default/add-delete.csv "plots/RDFS-Default/All versions of RETE/EPS/add-delete.eps" "plots/RDFS-Default/All versions of RETE/PNG/add-delete.png" --reasoners=rete
python draw_plots.py aggregated-results/RDFS-Default/add-delete.csv "plots/RDFS-Default/LiRoT vs Jena vs RDFox/EPS/add-delete.eps" "plots/RDFS-Default/LiRoT vs Jena vs RDFox/PNG/add-delete.png" --reasoners=all --log

# RDFS-Full

python draw_plots.py aggregated-results/RDFS-Full/full.csv "plots/RDFS-Full/All versions of RETE/EPS/full.eps" "plots/RDFS-Full/All versions of RETE/PNG/full.png" --reasoners=rete
python draw_plots.py aggregated-results/RDFS-Full/full.csv "plots/RDFS-Full/LiRoT vs Jena vs RDFox/EPS/full.eps" "plots/RDFS-Full/LiRoT vs Jena vs RDFox/PNG/full.png" --reasoners=all --log

python draw_plots.py aggregated-results/RDFS-Full/add.csv "plots/RDFS-Full/All versions of RETE/EPS/add.eps" "plots/RDFS-Full/All versions of RETE/PNG/add.png" --reasoners=rete
python draw_plots.py aggregated-results/RDFS-Full/add.csv "plots/RDFS-Full/LiRoT vs Jena vs RDFox/EPS/add.eps" "plots/RDFS-Full/LiRoT vs Jena vs RDFox/PNG/add.png" --reasoners=all --log

python draw_plots.py aggregated-results/RDFS-Full/delete.csv "plots/RDFS-Full/All versions of RETE/EPS/delete.eps" "plots/RDFS-Full/All versions of RETE/PNG/delete.png" --reasoners=rete
python draw_plots.py aggregated-results/RDFS-Full/delete.csv "plots/RDFS-Full/LiRoT vs Jena vs RDFox/EPS/delete.eps" "plots/RDFS-Full/LiRoT vs Jena vs RDFox/PNG/delete.png" --reasoners=all --log

python draw_plots.py aggregated-results/RDFS-Full/add-delete.csv "plots/RDFS-Full/All versions of RETE/EPS/add-delete.eps" "plots/RDFS-Full/All versions of RETE/PNG/add-delete.png" --reasoners=rete
python draw_plots.py aggregated-results/RDFS-Full/add-delete.csv "plots/RDFS-Full/LiRoT vs Jena vs RDFox/EPS/add-delete.eps" "plots/RDFS-Full/LiRoT vs Jena vs RDFox/PNG/add-delete.png" --reasoners=all --log