# scripts

This folder contains the scripts to run all reasoners and generate plots from their results. It has two subfolders:
 - ```reasoners```: scripts to run all reasoners; each reasoner has its own subfolder and instructions;
 - ```raw-to-plots```: scripts to generate plots from raw results files.
