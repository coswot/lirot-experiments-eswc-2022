# results-csv
For these experiments, one file is generated for each combination of dataset size, ruleset, reasoner and type of test.  

### Subfolder structure

This subfolder is organized as follows:
- [ruleset]/
	- full-stats.csv
	- add-stats.csv
	- delete-stats.csv
	- add-delete-stats.csv
	- [reasoner]/
		- [dataset size]/
			- full.csv
			- add.csv
			- delete.csv
			- add-delete.csv

The four types of CSV files correspond to each test described [here](https://gitlab.com/coswot/lirot-experiments-eswc-2022/-/blob/main/README.md).

### Structure of the raw results files
Results files show the execution time and memory footprint for each test. To measure these variables, we used the [tstime](https://github.com/gsauthof/tstime) tool. Hence the data contained in the raw results files are structured around the variables that ```tstime``` can measure. These variables are:
- **real time** (in ms): the total duration of the program execution; we used this variable to measure the execution time
- **user time** (in ms): the CPU time spent in user-mode within the process
- **system time** (in ms): the CPU time spent in the Linux kernel (hence outside of the process that is being monitored)
- **maximum resident set size (maxRSS)** (in KB): the maximum amout of RAM used by a process during its execution; this is what we used to measure memory footprint
- **maximum virtual memory size (maxVSZ)** (in KB): this includes all the memory space that a process has access to, even if it does not use it.

In all CSV files, each line is the result of a ```tstime``` call, hence conforming to the following structure:  
```real_time; user_time; sys_time; max_rss; max_vsz```

All tests are repeated 20 times, and mean values are placed in statistics files (see the following section).

### Statistics files
We aggregated all raw results files into a single file for each type of test (```full```, ```add```, ```delete``` and ```add-delete```) and for each ruleset (RDFS-Simple, RDFS-Default and RDFS-Full). These files are ```full-stats.csv```, ```add-stats.csv```, ```delete-stats.csv``` and ```add-delete-stats.csv```, and they are used to make the final plots.

In these files, each line shows average values for one reasoner and one dataset size, excluding the 5% extreme values. Hence they have the following columns:
```reasoner; dataset-size; real_time; user_time; sys_time; max_rss; max_vsz```

### ESP32
We also performed tests on a ESP32 board. We compared all versions of the RETE algorithm (with and without optimizations). Explicit facts were loaded in the reasoner incrementally. The CSV files in the ```ESP32``` subfolder show the amount of memory used for each number of explicit facts.
