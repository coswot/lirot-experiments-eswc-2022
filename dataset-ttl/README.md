# dataset-ttl

This folder contains the datasets generated with [LUBM](http://swat.cse.lehigh.edu/projects/lubm/) that were used for these experiments.
Dataset sizes range from 1 to 10,000 explicit facts.

The LUBM ontology is in a separate **univ-bench.ttl** file (it can also be found [online](http://swat.cse.lehigh.edu/onto/univ-bench.owl)).

This directory is organized as follows:

- [dataset size]/
	- parts/
		- part-[id].ttl
	- full.ttl
	- init.ttl

**full.ttl** contains the full generated dataset.  
**init.ttl** contains the first half of the dataset.

The **parts** subfolder (and subsequent **part-[id].ttl** files) contains fragments of the remaining half of the dataset. There are five fragments, and each fragment contains 10% of the whole dataset. This makes a partition of the dataset (hence all parts are disjoint and their union is equal to the whole dataset).

The same datasets are used for all reasoners and with all rulesets.
