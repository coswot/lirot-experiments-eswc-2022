# reduced-dataset

This subfolder contains a small dataset that was used to run tests on a ESP32 board. It was generated with [LUBM](http://swat.cse.lehigh.edu/projects/lubm/), using the [LUBM ontology](http://swat.cse.lehigh.edu/onto/univ-bench.owl). A fragment of the ontology (the [ontology.ttl](reduced-dataset/ontology.ttl) file) has been used for these experiments. This dataset is divided in 20 parts (in the [parts](reduced-dataset/parts/) folder), containing 20 explicit facts each. This allows to load a very small amount of facts at once until the device runs out of memory.
