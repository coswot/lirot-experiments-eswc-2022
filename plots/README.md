# plots
This folder contains the plots for all the tests that were performed. It has the following architecture:
- [ruleset]/
	- [reasoners]/
		- EPS/
		- PNG/

There is one plot per type of test (```full```, ```add```, ```delete``` and ```add-delete```, as described [here](https://gitlab.com/coswot/lirot-experiments-eswc-2022/-/blob/main/README.md), and per ruleset.  We compared LiRoT with Jena and RDFox (```LiRoT vs Jena vs RDFox``` subfolders), and RETE with and without optimizations (```All versions of RETE``` subfolders; this includes LiRoT).

This folder also includes the memory footprint plot on ESP32.

Plots are available in PNG format, as well as vector format (EPS).
